/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* detectCycle(struct ListNode* head)
{
    struct ListNode* slow, * fast;
    slow = fast = head;
    //因为一开始都为头结点肯定相等,所以要用do while
    do
    {
        if (fast == NULL || fast->next == NULL)
        {
            return NULL;
        }
        slow = slow->next;
        fast = fast->next->next;

    } while (slow != fast);
    //slow走的距离为fast的一半,slow再继续走,
    //再走一倍可以回到原来的位置
    //走完一倍之前可以回到入环点
    //再用head模拟slow第一次走,第一个相遇点即为入环点
    while (head != slow)
    {
        head = head->next;
        slow = slow->next;
    }
    return slow;
}