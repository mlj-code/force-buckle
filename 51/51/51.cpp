class Solution {
public:
    vector<vector<string>> ret;
    vector<string> ans;
    string tmp;
    int a[10];

    bool check(int cur)
    {
        for (int i = 0; i < cur; i++)
        {
            ////在同一纵向或同一斜线放置非法,因为一行只放一个不存在同行的情况
            if (a[i] == a[cur] ||
                abs((a[cur] - a[i])) == (cur - i)) return false;
        }
        return true;
    }

    void dfs(int n, int cur)
    {
        if (cur == n)
        {
            ret.push_back(ans);
            return;
        }

        for (int i = 0; i < n; i++)
        {
            a[cur] = i;
            if (check(cur) == false)continue;

            tmp[i] = 'Q';
            ans.push_back(tmp);
            tmp[i] = '.';

            dfs(n, cur + 1);
            ans.pop_back();
        }
    }
    vector<vector<string>> solveNQueens(int n)
    {
        tmp.resize(n, '.');
        dfs(n, 0);
        return ret;
    }
};