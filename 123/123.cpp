class Solution {
public:
    int maxProfit(vector<int>& p)
    {
        int y[500001] = { 0 };//还有一次买的机会,手里有股票
        int z[500001] = { 0 };//还有一次买的机会,手里没股票
        int m[500001] = { 0 };//没有买的机会,手里有
        int n[500001] = { 0 };//没有卖的机会,手里没
        int s = p.size();
        y[0] = -p[0];
        z[0] = 0;
        m[0] = -p[0];
        n[0] = 0;
        for (int i = 1; i < s; i++)
        {
            y[i] = max(-p[i], y[i - 1]);
            z[i] = max(z[i - 1], y[i - 1] + p[i]);
            m[i] = max(z[i - 1] - p[i], m[i - 1]);
            n[i] = max(n[i - 1], m[i - 1] + p[i]);
        }
        s = max(z[s - 1], n[s - 1]);
        if (s >= 0) return s;//额外一次都没有交易的情况
        else return 0;
    }
};