class Solution {
public:
    int numJewelsInStones(string jewels, string stones)
    {
        int ret = 0;
        unordered_set<char> set;
        for (int i = 0; i < jewels.size(); i++)set.insert(jewels[i]);
        for (int i = 0; i < stones.size(); i++)if (set.find(stones[i]) != set.end())ret++;
        return ret;
    }
};