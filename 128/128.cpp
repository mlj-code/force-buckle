class Solution {
public:
    int longestConsecutive(vector<int>& nums)
    {
        set<int> hash;
        for (auto e : nums)hash.insert(e);
        int ret = 0;
        int tmp = 1;

        for (auto it = hash.begin()++; it != hash.end(); it++)
        {
            auto last = it;
            last--;
            if (*it == *last + 1)tmp++;
            else
            {
                tmp = 1;
            }
            ret = max(ret, tmp);
        }
        return ret;
    }
};