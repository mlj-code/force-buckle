class Solution {
public:
    int maxProfit(vector<int>& prices)
    {
        int n = prices.size();
        int ret = 0;
        if (n < 2)return 0;

        int cur = 1;
        while (cur < n)
        {
            while (cur < n && prices[cur] <= prices[cur - 1])cur++;//先找到一个上升趋势
            int pre = prices[cur - 1];//固定极小值点

            while (cur < n && prices[cur] >= prices[cur - 1])cur++;//再找到一个下降趋势
            ret += prices[cur - 1] - pre;
        }
        return ret;
    }
};