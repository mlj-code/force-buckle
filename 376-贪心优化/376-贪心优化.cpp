class Solution {
public:
    int wiggleMaxLength(vector<int>& nums)
    {
        //贪心,数列的极值点及首尾点加上即为摆动序列
        int n = nums.size();
        if (n == 1)return 1;
        //表示左右差值,当同为正或同为负表示一个波峰或波谷
        int left = 0, right = 0;
        int cnt = 0;//极值点的个数
        for (int i = 1; i < n;)
        {
            left = right = 0;
            while (i < n && nums[i] - nums[i - 1] == 0) i++;
            if (i < n) left = nums[i] - nums[i - 1];

            int mid = i++;//可能的极值点
            while (i < n && nums[mid] - nums[i] == 0) i++;

            if (i < n) right = nums[mid] - nums[i];
            if (left * right > 0)
            {
                cnt++;
            }
        }
        //当一个波峰波谷都没有,且首尾相等,说明全部数据都一样,只有一个摆动序列
        if (cnt == 0 && nums[0] == nums[n - 1])cnt++;
        else cnt += 2;

        return cnt;

    }
};