class Solution {
public:
    int longestArithSeqLength(vector<int>& nums)
    {
        int ret = 2;
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n, 2));
        unordered_map<int, int> hash;
        hash[nums[0]] = 0;
        for (int j = 1; j < n; j++)
        {

            for (int i = j + 1; i < n; i++)
            {
                int before = 2 * nums[j] - nums[i];
                if (hash.count(before))
                {
                    dp[i][j] = dp[j][hash[before]] + 1;
                    ret = max(ret, dp[i][j]);
                }
            }
            hash[nums[j]] = j;
        }
        return ret;
    }
};
