class Solution {
public:
    int longestPalindrome(string s)
    {
        unordered_map<char, int> map;
        for (auto e : s)map[e]++;

        int ret = 0;
        bool flag = false;
        for (auto e : map)
        {
            if (e.second % 2 == 0)ret += e.second;
            else
            {
                flag = true;
                ret += e.second - 1;
            }
        }
        if (flag == true)ret++;
        return ret;
    }
};