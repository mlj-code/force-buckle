class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict)
    {
        unordered_set<string> dict;
        int n = s.size();
        for (auto& e : wordDict) dict.insert(e);//���ֵ�ֵ�����ϣ,�������
        bool dp[300] = { false };
        dp[0] = true;
        s = ' ' + s;
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= i; j++)
            {
                if (dp[j - 1] && dict.count(s.substr(j, i - j + 1)))
                {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
};