class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums)
    {
        int n = nums.size();
        int sum = 0;
        int dp[5000] = { 0 };
        dp[0] = 0; dp[1] = 0;
        //dp[i]表示每次新增子数组数
        for (int i = 2; i < n; i++)
        {
            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2])
            {
                dp[i] = dp[i - 1] + 1;
            }
            else dp[i] = 0;
            sum += dp[i];
        }
        return sum;
    }
};