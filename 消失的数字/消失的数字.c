#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

int missingNumber(int* nums, int numsSize)
{
    int sum = 0;
    for (int i = 1; i <= numsSize; i++)
    {
        sum += i;
    }
    for (int i = 0; i < numsSize; i++)
    {
        sum -= nums[i];
    }
    return sum;
}
