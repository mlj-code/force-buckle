class Solution {
public:
    string convertToTitle(int columnNumber)
    {
        string ret;
        while (columnNumber)//实际上是26个字母,对应26进制
        {
            columnNumber--;//将其对应到索引从0开始
            ret += columnNumber % 26 + 'A';
            columnNumber /= 26;
        }

        reverse(ret.begin(), ret.end());
        return ret;
    }
};