class Solution {
public:
    int a[10];
    void help(int n, int cnt)
    {
        for (int i = n - 1; i >= 0; i--)
        {
            if (i >= n - cnt) a[i] = 1;
            else a[i] = 0;
        }
    }
    vector<vector<int>> subsets(vector<int>& nums)
    {
        vector<vector<int>> ret;
        vector<int> tmp;
        int n = nums.size();

        for (int i = 0; i <= n; i++)
        {
            help(n, i);//获取最小子序列

            do {
                for (int j = 0; j < n; j++)
                {
                    if (a[j] == 1) tmp.push_back(nums[j]);
                }
                ret.push_back(tmp);
                tmp.clear();
            } while (next_permutation(a, a + n));
        }

        return ret;
    }
};