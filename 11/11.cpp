class Solution {
public:
    int maxArea(vector<int>& h)
    {
        int left = 0, right = h.size() - 1;
        int limit, ret;

        if (h[left] > h[right]) limit = h[right];
        else limit = h[left];

        ret = limit * (right - left);
        while (left < right)
        {
            if (limit == h[left])
            {
                while (limit >= h[left] && left < right)
                {
                    left++;
                }
                //当左指针突破自己的限制并且合法的时候就可以再计算一次
                if (left < right)
                {
                    limit = min(h[left], h[right]);
                    ret = max(ret, limit * (right - left));
                }
            }
            else
            {
                while (limit >= h[right] && left < right)
                {
                    right--;
                }
                if (left < right)
                {
                    limit = min(h[left], h[right]);
                    ret = max(ret, limit * (right - left));
                }
            }
        }
        return ret;
    }
};