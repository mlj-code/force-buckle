class Solution {
public:
    int strStr(string haystack, string needle)
    {
        for (int i = 0; i < haystack.size(); i++)
        {
            bool flag = true;
            if (haystack[i] != needle[0]) continue;

            for (int j = 1, k = i + 1; j < needle.size(); j++, k++)
            {
                if (haystack[k] != needle[j])
                {
                    flag = false;
                    break;
                }
            }

            if (flag == true) return i;
        }
        return -1;
    }
};