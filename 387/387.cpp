class Solution {
public:
    int firstUniqChar(string s)
    {
        int frequent[26] = { 0 };
        string::iterator it = s.begin();
        while (it != s.end())
        {
            frequent[*it - 'a']++;
            it++;
        }
        it = s.begin();
        int i = 0;
        while (it != s.end())
        {
            if (frequent[*it - 'a'] == 1)
            {
                return i;
            }
            i++;
            it++;
        }
        return -1;
    }
};