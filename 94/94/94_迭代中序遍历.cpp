/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> ret;
    vector<int> inorderTraversal(TreeNode* root)
    {
        if (root == nullptr) return ret;

        stack<TreeNode*> st;
        unordered_set<TreeNode*> set;//放遍历过的节点
        TreeNode* cur = root;
        do
        {
            //向左深度优先遍历,当发现节点为空或已经遍历过时退回上一层
            while (cur != nullptr && set.find(cur) == set.end())
            {
                st.push(cur);
                cur = cur->left;
            }

            //退回上一层
            cur = st.top();
            //上一层如果没有遍历过就入栈并标记,同时将遍历节点指向其右子树
            if (set.find(cur) == set.end())
            {
                ret.push_back(cur->val);
                set.insert(cur);
                cur = cur->right;
            }
            else//说明整棵子树已经遍历完,继续回退
            {
                st.pop();
                if (!st.empty())
                    cur = st.top();
                else break;
            }
        } while (!st.empty());

        return ret;
    }
};