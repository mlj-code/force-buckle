
class Solution {
public:
    string addStrings(string num1, string num2)
    {
        string::reverse_iterator pos1 = num1.rbegin();
        string::reverse_iterator pos2 = num2.rbegin();
        string ret;
        char digit;
        char carry = '0';
        while (pos1 != num1.rend() && pos2 != num2.rend())
        {
            digit = (*pos1 + *pos2 + carry) - 3 * '0';
            if (digit > 9)
            {
                digit = digit % 10 + '0';
                carry = '1';
            }
            else
            {
                digit += '0';
                carry = '0';
            }
            pos1++; pos2++;
            ret += digit;
        }
        while (pos1 != num1.rend())
        {
            digit = *pos1 + carry - 2 * '0';
            if (digit > 9)
            {
                digit = digit % 10 + '0';
                carry = '1';
            }
            else
            {
                digit += '0';
                carry = '0';
            }
            pos1++;
            ret += digit;
        }
        while (pos2 != num2.rend())
        {
            digit = *pos2 + carry - 2 * '0';
            if (digit > 9)
            {
                digit = digit % 10 + '0';
                carry = '1';
            }
            else
            {
                digit += '0';
                carry = '0';
            }
            ret += digit;
            pos2++;
        }
        if (carry == '1')
        {
            ret += '1';
        }
        return string(ret.rbegin(), ret.rend());
    }
};