class Solution {
public:
    int maxSubarraySumCircular(vector<int>& nums)
    {
        int f[30001];
        int g[30001];
        f[0] = g[0] = nums[0];
        int n = nums.size();
        int sum = nums[0];
        int big = f[0], small = g[0];
        for (int i = 1; i < n; i++)
        {
            f[i] = max(f[i - 1] + nums[i], nums[i]);
            sum += nums[i];
            if (f[i] > big)big = f[i];
        }
        for (int i = 1; i < n; i++)
        {
            g[i] = min(g[i - 1] + nums[i], nums[i]);
            if (g[i] < small)small = g[i];
        }
        if (small == sum)return big;
        big = max(big, sum - small);
        return big;
    }
};