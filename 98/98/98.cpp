/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    double max = (double)INT_MIN - 1;//�����
    bool isValidBST(TreeNode* root)
    {
        return _inorder(root);
    }
    //���������˳������
    bool _inorder(TreeNode* root)
    {
        if (root == nullptr) return true;

        bool left = _inorder(root->left);
        if (max < root->val) max = root->val;
        else return false;
        bool right = _inorder(root->right);
        return left && right;
    }
};