class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures)
    {
        int n = temperatures.size();
        stack<int> st;
        vector<int> ret(n, 0);
        for (int i = 0; i < n; i++)
        {
            //保持栈顶是当前遍历顺序的最大值下标值
            //当出栈时说明i位置是当前栈顶位置的下一个更大值,且相差i-st.top()
            while (!st.empty() && temperatures[i] > temperatures[st.top()])
            {
                ret[st.top()] = i - st.top();
                st.pop();
            }
            st.push(i);
        }
        return ret;
    }
};