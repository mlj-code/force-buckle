class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums)
    {
        int ret = INT_MAX;
        int n = nums.size();
        int tmp = 0;
        int left = 0, right = 0;
        while (right < n)
        {
            while (right < n && tmp < target)
            {
                tmp += nums[right++];
            }
            while (tmp >= target)
            {
                tmp -= nums[left++];
                ret = min(ret, right - left + 1);
            }
        }
        return ret == INT_MAX ? 0 : ret;
    }
};