/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<string> ret;
    vector<string> binaryTreePaths(TreeNode* root)
    {
        string tmp = to_string(root->val);
        if (root->left == nullptr && root->right == nullptr) return { tmp };
        _help(root->left, tmp);
        _help(root->right, tmp);

        return ret;
    }

    void _help(TreeNode* root, string& st)
    {
        if (root == nullptr) return;

        string tmp = st + "->" + to_string(root->val);

        _help(root->left, tmp);
        _help(root->right, tmp);

        if (root->left == nullptr && root->right == nullptr)
            ret.push_back(tmp);
    }
};