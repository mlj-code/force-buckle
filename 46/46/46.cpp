class Solution
{
public:
    unordered_map<int, bool> hash;
    vector<vector<int>> ret;
    vector<int> tmp;
    int n;

    void dfs(vector<int>& nums, int cur)
    {
        if (cur == n)
        {
            ret.push_back(tmp);
            return;
        }


        for (int i = 0; i < n; i++)
        {
            if (hash[nums[i]] == false) continue;
            tmp.push_back(nums[i]);
            hash[nums[i]] = false;
            dfs(nums, cur + 1);
            tmp.pop_back();
            hash[nums[i]] = true;
        }
    }
    vector<vector<int>> permute(vector<int>& nums)
    {
        for (auto e : nums) hash[e] = true;
        n = nums.size();
        dfs(nums, 0);
        return ret;
    }
};