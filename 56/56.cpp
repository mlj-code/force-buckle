class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals)
    {
        vector<vector<int>> ret;
        //�Ȱ���߽�����
        sort(intervals.begin(), intervals.end(),
            [](vector<int>& x, vector<int>& y) {
                return x[0] < y[0];
            });

        for (int i = 0; i < intervals.size(); i++)
        {
            int left = intervals[i][0], right = intervals[i][1];
            while (i + 1 < intervals.size() && right >= intervals[i + 1][0])
            {
                right = max(right, intervals[i + 1][1]);
                i++;
            }
            ret.emplace_back(vector<int>{left, right});

        }
        return ret;
    }
};