class Solution
{

public:

    int numSquares(int n)
    {
        //本质是容量为n的完全背包要求刚好完全装满
        //确保取最小值时不会取到非法值,将非法位置初始化为无穷大
        vector<int> dp(n + 1, INT_MAX);
        dp[0] = 0;
        for (int i = 1; i <= sqrt(n); i++)
        {
            for (int j = i * i; j <= n; j++)
            {
                dp[j] = min(dp[j], dp[j - i * i] + 1);
            }
        }
        return dp[n];
    }
};