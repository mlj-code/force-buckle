typedef int QDataType;

// 链式结构：表示队列 
typedef struct QListNode
{
	struct QListNode* _next;
	QDataType _data;
}QNode;

// 队列的结构 
typedef struct Queue
{
	QNode* _front;
	QNode* _rear;
}Queue;
// 初始化队列 
void QueueInit(Queue* q)
{
	assert(q);
	q->_front = q->_rear = NULL;
}
// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	QNode* tmp = (QNode*)malloc(sizeof(QNode));
	if (tmp == NULL)
	{
		perror("QNode malloc::fail\n");
		exit(-1);
	}
	else
	{
		tmp->_next = NULL;
		tmp->_data = data;
		if (q->_front == NULL)
		{
			q->_front = q->_rear = tmp;
		}
		else
		{
			q->_rear->_next = tmp;
			q->_rear = tmp;
		}
	}
}
// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	QNode* del = q->_front;
	q->_front = q->_front->_next;
	free(del);
}
// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->_front);
	return q->_front->_data;
}
// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->_rear);
	return q->_rear->_data;
}
// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	assert(q);
	assert(q->_front);
	QNode* cur = q->_front;
	int i = 0;
	while (cur)
	{
		i++;
		cur = cur->_next;
	}
	return i;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	assert(q);
	if (q->_front != NULL)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
// 销毁队列 
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->_front;
	while (cur)
	{
		QNode* del = cur;
		cur = cur->_next;
		free(del);
	}
	q->_front = q->_rear = NULL;
}


typedef struct
{
	Queue q1;
	Queue q2;
} MyStack;


MyStack* myStackCreate()
{
	MyStack* tmp = (MyStack*)malloc(sizeof(MyStack));
	QueueInit(&tmp->q1);
	QueueInit(&tmp->q2);
	return tmp;
}

void myStackPush(MyStack* obj, int x)
{
	if (!QueueEmpty(&obj->q1))
	{
		QueuePush(&obj->q1, x);
	}
	else
	{
		QueuePush(&obj->q2, x);
	}
}


void MoveOther(Queue* src, Queue* dest)
{

	while (src->_front != src->_rear)
	{
		int tmp = QueueFront(src);
		QueuePop(src);
		QueuePush(dest, tmp);
	}
}



int myStackPop(MyStack* obj)
{
	int ret = 0;
	if (!QueueEmpty(&obj->q1))
	{
		//剩一个元素其他全移到q2
		MoveOther(&obj->q1, &obj->q2);
		ret = QueueFront(&obj->q1);
		QueuePop(&obj->q1);
	}
	else
	{
		MoveOther(&obj->q2, &obj->q1);
		ret = QueueFront(&obj->q2);
		QueuePop(&obj->q2);
	}
	return ret;
}

int myStackTop(MyStack* obj)
{
	int ret = 0;
	if (!QueueEmpty(&obj->q1))
	{
		ret = QueueBack(&obj->q1);
	}
	else
	{
		ret = QueueBack(&obj->q2);
	}
	return ret;
}

bool myStackEmpty(MyStack* obj)
{
	if (QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void myStackFree(MyStack* obj)
{
	QueueDestroy(&obj->q1);
	QueueDestroy(&obj->q2);
	free(obj);
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/