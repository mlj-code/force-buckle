class Solution {
public:
    int minimumDeleteSum(string s1, string s2)
    {
        int m = s1.size(), n = s2.size();
        long long ret = 0;
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for (int i = 0; i < m; i++)
        {

            ret += s1[i];
        }
        for (int i = 0; i < n; i++)
        {
            //dp[0][i]=s2[i];
            ret += s2[i];
        }
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (s1[i - 1] == s2[j - 1])
                {
                    dp[i][j] = dp[i - 1][j - 1] + s1[i - 1];
                }
                else
                {
                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return ret - 2 * dp[m][n];

    }
};