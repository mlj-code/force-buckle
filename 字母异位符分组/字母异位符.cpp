class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs)
    {
        vector<vector<string>> ret;
        map<string, int> map;
        int i = 0;
        for (auto& e : strs)
        {
            string tmp = e;
            sort(tmp.begin(), tmp.end());
            if (map.find(tmp) != map.end())
            {
                ret[map[tmp]].push_back(e);
            }
            else
            {
                map[tmp] = i++;
                ret.push_back({ e });
            }
        }
        return ret;
    }
};