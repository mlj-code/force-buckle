/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
{
    struct ListNode* tailA, * tailB;
    tailA = headA;
    tailB = headB;
    //找出长度差
    int i = 0;
    int j = 0;
    while (tailA != tailB)
    {
        if (tailA == NULL)
        {
            i++;
            tailB = tailB->next;
        }
        else if (tailB == NULL)
        {
            j++;
            tailA = tailA->next;
        }
        else
        {
            tailA = tailA->next;
            tailB = tailB->next;
        }
    }
    //根据长度差让长的先行长度差位
    if (i)
    {
        for (; i > 0; i--)
        {
            headB = headB->next;
        }
        while (headA != headB)
        {
            headA = headA->next;
            headB = headB->next;
        }
        return headA;
    }
    if (j)
    {
        for (; j > 0; j--)
        {
            headA = headA->next;
        }
        while (headA != headB)
        {
            headA = headA->next;
            headB = headB->next;
        }
        return headA;
    }
    return tailA;
}