#if 0//优化前,先求最大利润

class Solution {
public:
    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit)
    {
        //先求最大利润
        int m = group.size();//工作数
        int ret = 0;
        vector<int> maxP(n + 1, 0);
        for (int i = 1; i <= m; i++)
        {
            for (int j = n; j >= group[i - 1]; j--)
            {
                maxP[j] = max(maxP[j], maxP[j - group[i - 1]] + profit[i - 1]);
            }
        }
        int x = maxP[n];//作为最大利润


        vector<vector<int>> dp(n + 1, vector<int>(x + 1, 0));
        for (int i = 0; i <= n; i++)dp[i][0] = 1;
        for (int i = 1; i <= m; i++)//工作维
        {
            for (int j = n; j >= group[i - 1]; j--)//员工数维
            {
                for (int k = x; k >= profit[i - 1]; k--)//利润维
                {
                    dp[j][k] += dp[j - group[i - 1]][k - profit[i - 1]];
                    dp[j][k] %= (1000000007);
                }
            }
        }
        for (int k = minProfit; k <= x; k++)
        {
            ret += dp[n][k];
            ret %= (1000000007);
        }
        return ret;
    }
};

#else if 1  //优化后
class Solution {
public:
    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit)
    {
        int m = group.size();
        vector<vector<int>> dp(n + 1, vector<int>(minProfit + 1, 0));
        //无论多少个人,想要利润为0,就只有不工作一种选法
        for (int i = 0; i <= n; i++)dp[i][0] = 1;

        for (int i = 0; i < m; i++)
            for (int j = n; j >= group[i]; j--)
                for (int k = minProfit; k >= 0; k--)
                {
                    //此处允许k-p[i]小于0的状态,因为此时P[I]大于K,满足利润至少大于k的状态定义,但是数组不存在这种状态,所以从0处取状态
                    dp[j][k] += dp[j - group[i]][max(0, k - profit[i])];
                    dp[j][k] %= 1000000007;
                }
        return dp[n][minProfit];
    }
};

#endif
