class Solution {
public:
    int rob(vector<int>& nums)
    {
        int n = nums.size();
        return max(nums[0] + rob1(nums, 2, n - 1), rob1(nums, 1, n));
    }

    int rob1(vector<int>& nums, int left, int right)
    {
        if (left >= right) return 0;
        int f[101];
        int g[101];
        f[left] = nums[left];
        g[left] = 0;
        for (int i = left + 1; i < right; i++)
        {
            f[i] = nums[i] + g[i - 1];
            g[i] = max(g[i - 1], f[i - 1]);
        }
        return max(f[right - 1], g[right - 1]);
    }
};