class Solution {
public:
    int countGoodStrings(int low, int high, int zero, int one)
    {
        int ret = 0;
        vector<int> dp(high + 1, 0);
        dp[zero] += 1;
        dp[one] += 1;
        for (int i = 1; i <= high; i++)
        {
            if (i - zero > 0) dp[i] += dp[i - zero];
            if (i - one > 0) dp[i] += dp[i - one];

            dp[i] %= (int)(1e9 + 7);

            if (i >= low)
            {
                ret += dp[i];
                ret %= (int)(1e9 + 7);
            }
        }

        return ret;
    }
};