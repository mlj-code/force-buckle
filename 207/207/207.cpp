class Solution {
public:
    int cnt;
    int dept;
    unordered_map<int, vector<int>> map;
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites)
    {
        cnt = 0;
        dept = prerequisites.size() + 1;
        for (auto e : prerequisites)map[e[0]].push_back(e[1]);
        vector<bool> stat(numCourses, false);
        for (int i = 0; i < numCourses; i++)
        {
            if (stat[i] == true) continue;

            if (_help(i, stat) == false) return false;
            dept = prerequisites.size() + 1;
        }
        return true;
    }

    bool _help(int i, vector<bool>& stat)
    {
        if (stat[i] == true) return true;//本身已经学过也返回学习完成
        if (map.count(i) == 0) return stat[i] = true;
        dept--;
        if (dept == 0) return false;

        for (auto e : map[i])
        {
            if (_help(e, stat) == false)//只要有一科前置课无法完成则此课也无法完成
                return false;
        }

        stat[i] = true;//到这里说明前置条件已经全部达成
        return true;
    }
};