/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
 /**
  * Note: The returned array must be malloced, assume caller calls free().
  */


void preOrder(struct TreeNode* root, int* returnSize, int* ret)
{
    if (root == NULL)
    {
        return;
    }
    else
    {
        ret[*returnSize] = root->val;
        (*returnSize)++;
    }

    preOrder(root->left, returnSize, ret);
    preOrder(root->right, returnSize, ret);
}

int* preorderTraversal(struct TreeNode* root, int* returnSize)
{
    int* ret = (int*)malloc(sizeof(int) * 100);
    *returnSize = 0;
    preOrder(root, returnSize, ret);

    return ret;
}