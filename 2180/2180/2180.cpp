class Solution {
public:
    bool check(int num)
    {

        int tmp = 0;
        while (num)
        {
            tmp += num % 10;
            num /= 10;
        }
        return tmp % 2 == 0;
    }
    int countEven(int num)
    {
        int cnt = 0;
        for (int i = 1; i <= num; i++)
        {
            if (check(i) == true)cnt++;
        }
        return cnt;
    }
};