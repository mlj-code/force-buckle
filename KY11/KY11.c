#include <stdio.h>
#include <stdlib.h>

typedef struct TreeNode
{
    char val;
    struct TreeNode* left;
    struct TreeNode* right;
}tree;

tree* PreCreatTree(char* input, int* i, int num)
{
    tree* t = (tree*)malloc(sizeof(tree));
    if (input[*i] == '#')
    {
        (*i)++;
        return NULL;
    }
    else
    {
        t->val = input[*i];
        (*i)++;
    }
    t->left = PreCreatTree(input, i, num);
    t->right = PreCreatTree(input, i, num);
    return t;
}

void InOrderTra(tree* t)
{
    if (t == NULL)
    {
        return;
    }
    InOrderTra(t->left);
    printf("%c ", t->val);
    InOrderTra(t->right);
}

int main()
{
    char input[100];
    int num = 0;
    while (scanf("%c", &input[num++]) != EOF);
    int i = 0;
    tree* t = PreCreatTree(input, &i, num);
    InOrderTra(t);
    return 0;
}