typedef struct
{
    int front;
    int rear;
    int* a;
    int k;
} MyCircularQueue;



MyCircularQueue* myCircularQueueCreate(int k)
{
    MyCircularQueue* ret = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    ret->front = ret->rear = 0;
    ret->a = (int*)malloc(sizeof(int) * (k + 1));
    ret->k = k;
    return ret;
}


bool myCircularQueueIsFull(MyCircularQueue* obj)
{
    return  obj->front == (obj->rear + 1 + obj->k + 1) % (obj->k + 1);
}


bool myCircularQueueIsEmpty(MyCircularQueue* obj)
{
    return obj->front == obj->rear;
}


bool myCircularQueueEnQueue(MyCircularQueue* obj, int value)
{
    if (myCircularQueueIsFull(obj))
    {
        return false;
    }
    else
    {
        if (obj->rear == obj->k + 1)
        {
            obj->rear = 0;
        }
        obj->a[obj->rear++] = value;
        return true;
    }
}


bool myCircularQueueDeQueue(MyCircularQueue* obj)
{
    if (myCircularQueueIsEmpty(obj))
    {
        return false;
    }
    else
    {
        if (obj->front == obj->k + 1)
        {
            obj->front = 0;
        }
        obj->front++;
        return true;
    }
}


int myCircularQueueFront(MyCircularQueue* obj)
{
    if (myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    return obj->a[obj->front];
}


int myCircularQueueRear(MyCircularQueue* obj)
{
    if (myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    return obj->a[obj->rear - 1];
}




void myCircularQueueFree(MyCircularQueue* obj)
{
    free(obj->a);
    free(obj);
}


/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);

 * bool param_2 = myCircularQueueDeQueue(obj);

 * int param_3 = myCircularQueueFront(obj);

 * int param_4 = myCircularQueueRear(obj);

 * bool param_5 = myCircularQueueIsEmpty(obj);

 * bool param_6 = myCircularQueueIsFull(obj);

 * myCircularQueueFree(obj);
*/