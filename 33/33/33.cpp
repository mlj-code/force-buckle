class Solution {
public:
    int search(vector<int>& nums, int target)
    {
        int left = 0, right = nums.size() - 1;
        while (left <= right)
        {
            int mid = (left + right) >> 1;
            if (nums[mid] == target) return mid;
            if (nums[left] <= nums[mid])//mid左边全部有序,右边可能无序
            {
                if (target >= nums[left] && target < nums[mid])//在左边有序区域
                {
                    right = mid - 1;
                }
                else//在右边可能无序区
                {
                    left = mid + 1;
                }
            }
            else
            {
                if (target > nums[mid] && target <= nums[right])//在右边有序区
                {
                    left = mid + 1;
                }
                else//在左边可能无序区
                {
                    right = mid - 1;
                }
            }
        }
        return -1;
    }
};