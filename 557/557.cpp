class Solution {
public:
    string reverseWords(string s)
    {
        s.push_back(' ');
        int left = 0;
        int right;
        int len = s.size();
        cout << len << ' ';
        while (left < len)
        {
            right = s.find_first_of(' ', left);
            int i = left, j = right - 1, k = (right - left) / 2;
            while (k--)
            {
                char tmp = s[i];
                s[i] = s[j];
                s[j] = tmp;
                i++; j--;
            }
            left = right + 1;
        }
        s.pop_back();
        return s;
    }
};