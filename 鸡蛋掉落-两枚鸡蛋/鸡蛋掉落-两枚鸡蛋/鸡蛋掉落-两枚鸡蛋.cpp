class Solution {
public:
    int twoEggDrop(int n)
    {
        int ret = 0;
        while (n - ret > 0)
        {
            n -= ret;
            ret++;
        }

        return ret;
    }
};