class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& matrix)
    {
        int n = matrix.size();
        vector<vector<int>> dp(n + 1, vector(n + 2, INT_MAX));
        for (int i = 1; i <= n; i++)dp[1][i] = matrix[0][i - 1];
        for (int i = 2; i <= n; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = matrix[i - 1][j - 1] + min(min(dp[i - 1][j], dp[i - 1][j - 1]), dp[i - 1][j + 1]);
            }
        }
        int min = dp[n][1];
        for (int i = 1; i <= n; i++)
        {
            if (dp[n][i] < min) min = dp[n][i];
        }
        return min;
    }
};