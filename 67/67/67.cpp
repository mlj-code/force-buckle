class Solution {
public:
    string addBinary(string a, string b)
    {
        if (a.size() < b.size()) a.swap(b);
        int carry = 0, i, j;
        for (i = b.size() - 1, j = a.size() - 1; i >= 0; i--, j--)
        {
            int tmp = a[j] - '0' + b[i] - '0' + carry;
            a[j] = tmp % 2 + '0';
            carry = tmp / 2;
        }

        while (carry != 0 && j >= 0)
        {
            int tmp = a[j] - '0' + carry;
            a[j] = tmp % 2 + '0';
            carry = tmp / 2;
            j--;
        }

        if (carry != 0)//说明最后还有一个进位
        {
            a.insert(a.begin(), '1');
        }

        return a;
    }
};