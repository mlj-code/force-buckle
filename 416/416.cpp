#if 0  //优化前
class Solution {
public:
    bool canPartition(vector<int>& nums)
    {
        int n = nums.size(), sum = 0;
        for (auto e : nums)sum += e;
        if (sum % 2 == 1)return false;
        sum /= 2;
        vector<vector<bool>> dp(n + 1, vector<bool>(sum + 1, true));

        for (int j = 1; j <= sum; j++)dp[0][j] = false;//对于不存在的情况设为false

        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= sum; j++)
            {
                dp[i][j] = dp[i - 1][j];
                if (j >= nums[i - 1])
                    dp[i][j] = dp[i][j] || dp[i - 1][j - nums[i - 1]];
            }
        }
        return dp[n][sum];
    }
};

#else if 1  //优化后

class Solution {
public:
    bool canPartition(vector<int>& nums)
    {
        int n = nums.size(), sum = 0;
        for (auto e : nums)sum += e;
        if (sum % 2 == 1)return false;
        sum /= 2;
        vector<bool> dp(sum + 1, true);

        for (int j = 1; j <= sum; j++)dp[j] = false;//对于不存在的情况设为false

        for (int i = 1; i <= n; i++)
        {
            for (int j = sum; j >= nums[i - 1]; j--)
            {
                dp[j] = dp[j] || dp[j - nums[i - 1]];
            }
        }
        return dp[sum];
    }
};

#endif