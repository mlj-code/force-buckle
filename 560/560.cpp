#if 0//优化前

class Solution {
public:
    int subarraySum(vector<int>& nums, int k)
    {
        int n = nums.size(), ret = 0;
        vector<int> dp(n, 0);
        dp[0] = nums[0];
        if (dp[0] == k)ret++;
        for (int i = 1; i < n; i++)
        {
            dp[i] = dp[i - 1] + nums[i];
            if (dp[i] == k)ret++;
        }

        for (int i = 1; i < n; i++)
        {
            for (int j = i; j < n; j++)
            {
                dp[j] = dp[j] - nums[i - 1];
                if (dp[j] == k)ret++;
            }
        }
        return ret;
    }
};

#else if 1//优化后,前缀和加哈希

class Solution {
public:
    int subarraySum(vector<int>& nums, int k)
    {
        unordered_map<int, int> map;
        map[0] = 1;
        int pre = 0;//存放前缀和
        int ret = 0;
        for (auto& e : nums)
        {
            pre += e;
            if (map.find(pre - k) != map.end())
            {
                //加上符合当前位置前缀和减去前缀和为pre-k出现的次数
                ret += map[pre - k];
            }
            map[pre]++;
        }
        return ret;
    }
};

#endif