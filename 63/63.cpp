class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid)
    {
        int dp[101][101] = { 0 };
        dp[0][1] = 1;
        for (int i = 1; i <= obstacleGrid.size(); i++)
        {
            for (int j = 1; j <= obstacleGrid[i - 1].size(); j++)
            {
                if (obstacleGrid[i - 1][j - 1] != 1)
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[obstacleGrid.size()][obstacleGrid[obstacleGrid.size() - 1].size()];
    }
};