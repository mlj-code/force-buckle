class Solution {
public:
    string largestNumber(vector<int>& nums)
    {
        vector<string> str;
        for (auto e : nums)str.push_back(to_string(e));
        //贪心,让每个局部都最大,最终结果也最大
        sort(str.begin(), str.end(), [](string& str1, string& str2) {
            if (str1 + str2 > str2 + str1) return true;
            else return false;
            });

        string ret;
        for (auto& e : str)ret += e;
        if (ret[0] == '0') return "0";
        return ret;
    }
};