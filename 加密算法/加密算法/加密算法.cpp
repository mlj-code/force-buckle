#include <iostream>
#include <string>

using namespace std;

using ll = long long;

string num;

ll ans = 0;
int n;

ll dp[5000005];



int main()
{

    cin >> n >> num;
    if (n == 0)
    {
        cout << 0 << endl;
        return 0;
    }
    else if (n == 1)
    {
        cout << 1 << endl;
        return 0;
    }
    else if (n == 2)
    {

        return 0;
    }

    dp[0] = 1;
    ll tmp = stol(num.substr(0, 2));
    if (tmp < 27 && tmp>0) dp[1] = 2;
    else dp[1] = 1;

    for (int i = 2; i < n; i++)
    {
        dp[i] = dp[i - 1];

        ll tmp = stol(num.substr(i - 1, 2));
        if (tmp < 27 && tmp>0) dp[i] += dp[i - 2];
        dp[i] %= 1000000007;;
    }

    cout << dp[n - 1] << endl;
    return 0;
}