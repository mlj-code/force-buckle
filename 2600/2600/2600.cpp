class Solution {
public:
    int kItemsWithMaximumSum(int numOnes, int numZeros, int numNegOnes, int k)
    {
        if (numOnes >= k) return k;
        else if (numZeros + numOnes >= k) return numOnes;
        else return numOnes - (k - numOnes - numZeros);
    }
};