#if 0 //红黑树
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k)
    {
        vector<int> ret;
        multiset<int> set(nums.begin(), nums.begin() + k);
        ret.push_back(*set.rbegin());
        for (int i = k; i < nums.size(); i++)
        {
            set.erase(set.find(nums[i - k]));
            set.insert(nums[i]);
            ret.push_back(*set.rbegin());
        }
        return ret;
    }
};

#else if 0 //优先级队列
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        int n = nums.size();
        priority_queue<pair<int, int>> q;
        for (int i = 0; i < k; ++i) {
            q.emplace(nums[i], i);
        }
        vector<int> ans = { q.top().first };
        for (int i = k; i < n; ++i) {
            q.emplace(nums[i], i);
            while (q.top().second <= i - k) {
                q.pop();
            }
            ans.push_back(q.top().first);
        }
        return ans;
    }
};

#else if 1//单调队列

class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k)
    {
        deque<int> q;//尾端放小的元素
        for (int i = 0; i < k; i++)
        {
            //将所有小于要插入元素的尾部元素移除
            while (!q.empty() && nums[i] >= nums[q.back()])
                q.pop_back();
            q.push_back(i);
        }
        //将头部最大元素插入
        vector<int> ret;
        ret.push_back(nums[q.front()]);
        for (int i = k; i < nums.size(); i++)
        {
            //将所有小于要插入元素的尾部元素移除
            while (!q.empty() && nums[i] >= nums[q.back()])
                q.pop_back();
            q.push_back(i);
            //将所有小于左边界的非法值移除,
            //因为队列中必定存在一个i,大于i-k所以无越界风险
            while (q.front() <= i - k)q.pop_front();

            ret.push_back(nums[q.front()]);
        }
        return ret;
    }
};

#endif