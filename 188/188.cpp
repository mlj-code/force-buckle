class Solution {
public:
    int maxProfit(int k, vector<int>& prices)
    {
        int n = prices.size();
        vector<vector<int>> dp(2 * k, vector<int>(n + 1, 0));
        for (int i = 0; i < 2 * k; i++)
        {
            if (i % 2 == 0)dp[i][0] = -prices[0];
            else dp[i][0] = 0;
        }
        for (int i = 1; i < n; i++)//天数
        {
            dp[0][i] = max(-prices[i], dp[0][i - 1]);//交易一次
            for (int j = 1; j < 2 * k; j++)//次数
            {
                if (j % 2 == 0)dp[j][i] = max(dp[j - 1][i - 1] - prices[i], dp[j][i - 1]);//第j天手里有股票
                else dp[j][i] = max(dp[j - 1][i - 1] + prices[i], dp[j][i - 1]);//第j天手里没股票
            }
        }
        int ret = 0;
        for (int i = 0; i < 2 * k; i++)
        {
            if (dp[i][n - 1] > ret) ret = dp[i][n - 1];
            cout << dp[i][n - 1] << ' ';
        }
        //cout<<dp[1][1];

        return ret;
    }
};