class Solution {
public:
    vector<int> getRow(int rowIndex)
    {
        vector<int> tmp1 = { 1 };
        vector<int> tmp2 = { 1,1 };

        if (rowIndex == 0) return tmp1;
        else if (rowIndex == 1) return tmp2;

        for (int i = 2; i <= rowIndex; i++)
        {
            tmp1.resize(i + 1);
            for (int j = 1; j < i; j++)
            {
                tmp1[j] = tmp2[j - 1] + tmp2[j];
            }
            tmp1[i] = 1;
            tmp2.swap(tmp1);
        }

        return tmp2;
    }
};