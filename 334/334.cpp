class Solution {
public:
    bool increasingTriplet(vector<int>& nums)
    {
        vector<int> ret;
        ret.push_back(nums[0]);
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] > ret.back())
            {
                ret.push_back(nums[i]);
                if (ret.size() == 3)return true;
            }
            else
            {
                int left = 0, right = ret.size() - 1;
                while (left < right)
                {
                    int mid = (left + right) >> 1;
                    if (ret[mid] < nums[i]) left = mid + 1;
                    else right = mid;
                }
                ret[left] = nums[i];
            }
        }
        return false;
    }
};