class Solution {
public:
    unordered_map<int, int> cnt, ori;
    bool check()
    {
        for (auto& p : ori)
        {
            //如果目标的字符出现次数比统计的少,说明滑动窗口非法
            if (cnt[p.first] < p.second)
                return false;
        }
        return true;
    }
    string minWindow(string s, string t)
    {
        for (auto e : t)ori[e]++;//将目标字符串存入哈希方便搜索

        int left = 0, right = 0, len = INT_MAX, ret_left = 0;
        while (right < s.size())
        {
            cnt[s[right++]]++;

            //当滑动窗口合法时尝试一直右移左边界,减小长度
            if (cnt[s[left]] >= ori[s[left]])//当单签字母合法时才有可能合法
                if (check() == true && left < right)
                {
                    while (left < right)
                    {
                        if (len > right - left)
                        {
                            ret_left = left;
                            len = right - left;
                        }

                        //左边界右移
                        cnt[s[left]]--;
                        left++;
                        if (ori.find(s[left - 1]) != ori.end())
                        {
                            if (cnt[s[left - 1]] < ori[s[left - 1]])
                                break;
                        }
                    }
                }
        }
        return len == INT_MAX ? "" : s.substr(ret_left, len);
    }
};