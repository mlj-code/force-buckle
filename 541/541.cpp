//class Solution {
//public:
//    string reverseStr(string s, int k)
//    {
//        if (s.size() < 2)
//        {
//            return s;
//        }
//        string::iterator it = s.begin();
//        string::iterator begin = it;
//        string::iterator cur;
//        while (it != s.end())
//        {
//            int i = k - 1;
//            begin = it;
//            while (i-- && it != s.end() - 1)
//            {
//                it++;
//            }
//            cur = it;
//            i = (cur - begin + 2) / 2;
//            while (i--)
//            {
//                char tmp = *begin;
//                *begin = *cur;
//                *cur = tmp;
//                cur--; begin++;
//            }
//            cout << s;
//            i = k + 1;
//            while (it != s.end() && i--)
//            {
//                it++;
//            }
//        }
//        return s;
//    }
//};

class Solution {
public:
    string reverseStr(string s, int k)
    {

        int curr_finsh = 0;
        int len = s.size();
        int left, right, next_curr;
        while (curr_finsh < len)
        {
            left = curr_finsh;
            right = min(left + k - 1, len - 1);
            next_curr = right + k + 1;
            while (left < right) { swap(s[left++], s[right--]); }
            curr_finsh = next_curr;
        }
        return s;
    }
};