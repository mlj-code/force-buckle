class Solution {
public:
    int jump(vector<int>& nums)
    {
        int n = nums.size();
        int cnt = 0, end = 0, next = 0;
        for (int i = 0; i <= end; i++)
        {
            if (end >= n - 1) return cnt;

            next = max(next, nums[i] + i);

            if (i == end)//在当前可到达范围内寻找下一个最远可到达的地方
            {
                end = next;
                cnt++;
            }
        }

        return -1;
    }
};