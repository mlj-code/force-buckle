class Solution {
public:
    int findMin(vector<int>& nums)
    {
        int left = 0, right = nums.size() - 1;
        while (left < right)//不管旋转几次,旋转后数组都可以分为左右两段均为递增
        {
            int mid = (left + right) >> 1;
            if (nums[mid] <= nums[right])
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }
        }
        return nums[left];
    }
};