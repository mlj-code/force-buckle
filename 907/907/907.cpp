class Solution {
public:
    int sumSubarrayMins(vector<int>& arr)
    {
        //找到一当前元素为最小值的子数组个数
        long long ans = 0;
        int n = arr.size();
        vector<int> left(n, -1);//表示arr[i]左边第一个比其小的元素下标
        vector<int> right(n, n);//表示arr[i]左边第一个比其小的元素下标

        stack<int> st;
        for (int i = 0; i < n; i++)
        {
            while (!st.empty() && arr[i] <= arr[st.top()])
            {
                right[st.top()] = i;
                st.pop();
            }
            st.push(i);
        }
        while (!st.empty()) st.pop();
        for (int i = n - 1; i >= 0; i--)
        {
            while (!st.empty() && arr[i] < arr[st.top()])
            {
                left[st.top()] = i;
                st.pop();
            }
            st.push(i);
        }

        for (int i = 0; i < n; i++)
        {
            int l = left[i];//左边第一个比arr[i]小的数
            int r = right[i];//右边第一个比arr[i]小的数

            long long tmp = (long long)arr[i] * (i - l) % 1000000007 * (r - i) % 1000000007;
            ans = (ans + tmp) % 1000000007;
        }

        return ans;
    }
};