class Solution {
public:
    bool isInterleave(string s1, string s2, string s3)
    {
        if (s1.size() + s2.size() != s3.size())return false;

        s1 = " " + s1, s2 = " " + s2, s3 = " " + s3;
        int m = s1.size(), n = s2.size();

        vector<vector<bool>> dp(m, vector<bool>(n, false));
        for (int i = 0; i < m; i++)
        {
            if (s1[i] == s3[i])dp[i][0] = true;
            else break;
        }
        for (int i = 0; i < n; i++)
        {
            if (s2[i] == s3[i])dp[0][i] = true;
            else break;
        }
        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                if (s2[j] == s3[i + j] && dp[i][j - 1] == true ||
                    s1[i] == s3[i + j] && dp[i - 1][j])
                    dp[i][j] = true;
            }
        }
        return dp[m - 1][n - 1];
    }
};