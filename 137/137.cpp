class Solution {
public:
    int singleNumber(vector<int>& nums)
    {
        int b = 1;
        int ret = 0;
        int mask; int cnt;
        for (int i = 0; i < 32; i++)
        {
            cnt = 0; mask = 1 << i;
            for (int j = 0; j < nums.size(); j++)
            {
                if (mask & nums[j])
                {
                    cnt++;
                }
            }
            if (cnt % 3 == 1)
            {
                ret |= mask;
            }
        }
        return ret;
    }
};