class Solution {
public:
    vector<int> sortJumbled(vector<int>& mapping, vector<int>& nums)
    {
        int n = nums.size();
        vector<int> tmp, ret, _sort;
        for (int i = 0; i < n; i++)
        {
            _sort.push_back(i);
            string str = to_string(nums[i]);
            for (int j = 0; j < str.size(); j++)
            {
                str[j] = mapping[str[j] - '0'] + '0';
            }
            tmp.push_back(stoi(str));
        }

        stable_sort(_sort.begin(), _sort.end(), [&](int x, int y)
            {
                return tmp[x] < tmp[y];
            });

        for (int i = 0; i < n; i++) ret.push_back(nums[_sort[i]]);

        return ret;


    }
};