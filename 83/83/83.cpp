/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head)
    {
        map<int, ListNode*> map;
        ListNode* cur = head, * ret = new ListNode();
        while (cur != nullptr)
        {
            map[cur->val] = cur;
            cur = cur->next;
        }
        cur = ret;
        for (auto e : map)
        {
            cur->next = e.second;
            cur = cur->next;
        }
        cur->next = nullptr;
        return ret->next;
    }
};