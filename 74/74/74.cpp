class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target)
    {
        int left = 0, right = matrix.size() * matrix[0].size();
        int wide = matrix[0].size();
        while (left < right)
        {
            int mid = (left + right) / 2;
            int tmp = matrix[mid / wide][mid % wide];
            if (tmp < target) left = mid + 1;
            else if (tmp > target) right = mid;
            else return true;
        }
        return false;
    }
};