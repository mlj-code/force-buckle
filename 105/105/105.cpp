/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int cnt;
    unordered_map<int, int> hash;
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder)
    {
        cnt = 0;
        for (int i = 0; i < inorder.size(); i++) hash[inorder[i]] = i;
        TreeNode* ret;
        helper(preorder, inorder, 0, inorder.size() - 1, ret);
        return ret;
    }

    void helper(vector<int>& preorder, vector<int>& inorder, int left, int right, TreeNode*& root)
    {
        if (left > right) return;
        int cur = hash[preorder[cnt]];//通过确定根节点在中序中的位置来确定左右边界
        TreeNode* tmp = new TreeNode(preorder[cnt++]);
        root = tmp;

        helper(preorder, inorder, left, cur - 1, root->left);
        helper(preorder, inorder, cur + 1, right, root->right);

    }
};