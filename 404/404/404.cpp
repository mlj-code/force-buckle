/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int ret = 0;
    int sumOfLeftLeaves(TreeNode* root)
    {
        _help(root->left, true);
        _help(root->right, false);
        return ret;
    }

    void _help(TreeNode* root, bool flag)
    {
        if (root == nullptr) return;
        if (root->left == nullptr && root->right == nullptr && flag)
        {
            ret += root->val;
            return;
        }

        _help(root->left, true);
        _help(root->right, false);
    }
};