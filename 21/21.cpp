/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
{
    struct ListNode* head, * tail;
    head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
    head->next = NULL;
    while (list1 && list2)
    {
        if (list1->val < list2->val)
        {
            tail->next = list1;
            list1 = list1->next;
            tail = tail->next;
        }
        else if (list1->val > list2->val)
        {
            tail->next = list2;
            list2 = list2->next;
            tail = tail->next;
        }
        else
        {
            tail->next = list1;
            list1 = list1->next;
            tail = tail->next;
        }
    }
    if (list2)
    {
        tail->next = list2;
    }
    if (list1)
    {
        tail->next = list1;
    }
    return head->next;
}