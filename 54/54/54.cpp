class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix)
    {
        int m = matrix.size(), n = matrix[0].size();

        vector<int> ret;
        int u = 0, d = m - 1, l = 0, r = n - 1;//上下左右边界
        while (true)
        {
            for (int i = l; i <= r; i++)
            {
                ret.push_back(matrix[u][i]);
            }
            if (++u > d)break;//从左向右遍历完上边界往下缩减,如果越界结束
            for (int i = u; i <= d; i++)
            {
                ret.push_back(matrix[i][r]);
            }
            if (--r < l)break;
            for (int i = r; i >= l; i--)
            {
                ret.push_back(matrix[d][i]);
            }
            if (--d < u)break;
            for (int i = d; i >= u; i--)
            {
                ret.push_back(matrix[i][l]);
            }
            if (++l > r)break;
        }
        return ret;
    }
};