class Solution {
public:
    int minRectanglesToCoverPoints(vector<vector<int>>& points, int w)
    {
        //只用处理x轴上的信息即可
        int ret = 0;
        vector<int> x;
        for (auto& point : points)
        {
            x.push_back(point[0]);
        }
        sort(x.begin(), x.end());

        int n = x.size();
        for (int i = 0; i < n;)
        {
            int start = x[i];
            while (i < n && start + w >= x[i])
            {
                i++;
            }
            ret++;
        }

        return ret;
    }
};