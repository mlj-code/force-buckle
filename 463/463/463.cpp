class Solution {
public:
    vector<vector<bool>> vist;
    int m;
    int n;
    int islandPerimeter(vector<vector<int>>& grid)
    {
        m = grid.size(), n = grid[0].size();
        int ret = 0;
        int i, j;
        for (i = 0; i < m; i++)
        {
            for (j = 0; j < n; j++)
            {
                if (grid[i][j] == 1)
                {
                    ret += _help(grid, i, j);
                    break;//一片岛屿,所以可以直接退出
                }
            }
        }

        return ret;
    }

    int _help(vector<vector<int>>& grid, int i, int j)
    {
        if (i < 0 || i >= m || j < 0 || j >= n || grid[i][j] == 0) return 1;//到达边界返回1
        if (grid[i][j] == 2) return 0;//访问过的退回
        grid[i][j] = 2;

        return _help(grid, i + 1, j) + _help(grid, i, j + 1) +
            _help(grid, i - 1, j) + _help(grid, i, j - 1);
    }
};