class Solution {
public:
    int halveArray(vector<int>& nums)
    {
        double target = 0;
        priority_queue<double> q;
        for (auto x : nums)
        {
            q.push(x);
            target += (double)x / 2;
        }
        cout << target;
        int ret = 0;
        while (target > 0)
        {
            double tmp = (double)q.top() / 2;
            target -= tmp;
            ret++;
            q.pop();
            q.push(tmp);
        }
        return ret;

    }
};