class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> ret(n, 0);
        int total = 1;
        int zero = -1;
        for (int i = 0; i < n; i++)
        {
            if (nums[i] == 0)
            {
                if (zero == -1) zero = i;
                else return ret;
            }
            else total *= nums[i];
        }

        if (zero != -1)
        {
            ret[zero] = total;
            return ret;
        }

        for (int i = 0; i < n; i++)
        {
            ret[i] = total / nums[i];
        }
        return ret;
    }
};