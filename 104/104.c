///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     struct TreeNode *left;
// *     struct TreeNode *right;
// * };
// */
//
//
//int maxDepth(struct TreeNode* root)
//{
//    if (root == NULL)
//    {
//        return 0;
//    }
//    int left = maxDepth(root->left);
//    int right = maxDepth(root->right);
//    int ret = left > right ? left : right;
//    return 1 + ret;
//}
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
typedef struct TreeNode TreeNode;

void InOrder(TreeNode* root, int* max, int deep) {
    if (root == NULL) return;
    InOrder(root->left, max, deep + 1);
    if (root->left == NULL && root->right == NULL) {
        if (deep > (*max))(*max) = deep;
    }
    InOrder(root->right, max, deep + 1);
}

int maxDepth(struct TreeNode* root) {
    if (root == NULL)return 0;
    int max = 1;
    InOrder(root, &max, 1);
    return max;
}