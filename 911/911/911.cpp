class TopVotedCandidate {
public:
    int n;
    vector<pair<int, int>> leading;
    TopVotedCandidate(vector<int>& persons, vector<int>& times)
    {
        n = persons.size();
        unordered_map<int, int> cnt;
        cnt[-1] = -1;
        int leader = -1;//记录暂时领先的人
        for (int i = 0; i < n; i++)
        {
            cnt[persons[i]]++;
            if (cnt[persons[i]] >= cnt[leader])
            {
                leader = persons[i];
            }
            leading.push_back({ times[i],leader });
        }
    }

    int q(int t)
    {
        int l = 0, r = n;
        while (l < r)
        {
            int mid = (l + r) >> 1;

            if (leading[mid].first == t) return leading[mid].second;
            else if (leading[mid].first > t) r = mid;
            else l = mid + 1;
        }



        if (l > 0) return leading[l - 1].second;
        else return leading[0].second;
    }
};

/**
 * Your TopVotedCandidate object will be instantiated and called as such:
 * TopVotedCandidate* obj = new TopVotedCandidate(persons, times);
 * int param_1 = obj->q(t);
 */