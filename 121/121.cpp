class Solution {
public:
    int maxProfit(vector<int>& prices)
    {
        int n = prices.size();
        if (n < 2)return 0;

        int min = prices[0], ret = prices[1] - prices[0];

        for (int i = 2; i < n; i++)
        {
            min = std::min(min, prices[i - 1]);
            ret = max(ret, prices[i] - min);
        }
        return max(ret, 0);
    }
};