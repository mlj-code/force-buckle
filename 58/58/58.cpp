class Solution {
public:
    int lengthOfLastWord(string s)
    {
        bool flag = false;
        int end = s.size();
        for (int i = s.size() - 1; i >= 0; i--)
        {
            if (s[i] == ' ')
            {
                if (flag == true) return end - i;
            }
            else if (flag == false) flag = true, end = i;
        }
        if (flag == true) return end + 1;//end指向最后一个字母
        else return -1;
    }
};