class Solution {
public:
    string removeDuplicates(string s)
    {
        string ret;
        int n = s.size();
        for (int i = 0; i < n; i++)
        {
            if (ret.empty() || ret.back() != s[i])
            {
                ret.push_back(s[i]);
            }
            else
            {
                ret.pop_back();
            }
        }

        return ret;
    }
};