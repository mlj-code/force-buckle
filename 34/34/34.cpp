class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target)
    {
        vector<int> ret;
        int left = 0, right = nums.size() - 1;
        int low = -1, hight = -1;

        while (left <= right)//寻找第一个
        {
            int mid = (left + right) >> 1;

            if (nums[mid] < target) left = mid + 1;
            else if (nums[mid] > target) right = mid - 1;
            else
            {
                right = mid - 1;
                low = mid;
            }
        }

        left = 0, right = nums.size() - 1;

        while (left <= right)//寻找第二个
        {
            int mid = (left + right) >> 1;

            if (nums[mid] < target) left = mid + 1;
            else if (nums[mid] > target) right = mid - 1;
            else
            {
                left = mid + 1;
                hight = mid;
            }
        }

        return { low,hight };
    }
};