class Solution {
public:
    int findNumberOfLIS(vector<int>& nums)
    {
        int ret = 0;
        int n = nums.size();
        int max_len = 0;
        vector<vector<int>> dp(n + 1, vector<int>(2, 1));
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[i] > nums[j])
                {
                    if (dp[i][0] < dp[j][0] + 1)
                    {
                        dp[i][0] = dp[j][0] + 1;
                        dp[i][1] = dp[j][1];
                    }
                    else if (dp[i][0] == dp[j][0] + 1)
                    {
                        dp[i][1] += dp[j][1];
                    }
                }
            }
            if (dp[i][0] > max_len)
            {
                ret = dp[i][1];
                max_len = dp[i][0];
            }
            else if (dp[i][0] == max_len) ret += dp[i][1];
        }

        return ret;
    }
};`