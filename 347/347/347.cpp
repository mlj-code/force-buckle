class Solution {
public:
    struct cmp
    {
        bool operator()(const pair<int, int>& x, const pair<int, int>& y)
        {
            return x.second < y.second;
        }
    };
    vector<int> topKFrequent(vector<int>& nums, int k)
    {
        unordered_map<int, int> cnt;
        for (auto x : nums) cnt[x]++;

        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> p;

        for (auto& x : cnt) p.push(x);

        vector<int> ret;

        while (k--)
        {
            ret.push_back(p.top().first);
            p.pop();
        }

        return ret;
    }
};