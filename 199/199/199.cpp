/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root)
    {
        return helper(root);
    }

    vector<int> helper(TreeNode* root)
    {
        if (root == nullptr) return {};
        vector<int> ret;
        ret.push_back(root->val);
        //优先向右深度遍历,如果左边的长则截取长的一段拼接上
        vector<int> tmp1 = helper(root->right);
        vector<int> tmp2 = helper(root->left);

        ret.insert(ret.end(), tmp1.begin(), tmp1.end());

        if (tmp2.size() > tmp1.size())
        {
            ret.insert(ret.end(), tmp2.begin() + tmp1.size(), tmp2.end());
        }

        return ret;
    }
};