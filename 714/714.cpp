class Solution {
public:
    int maxProfit(vector<int>& prices, int fee)
    {
        int n = prices.size();
        int f[50001] = { 0 };//表示当天结束后手里有股票
        int g[50001] = { 0 };//没有
        f[0] = -prices[0];
        g[0] = 0;
        for (int i = 1; i < n; i++)
        {
            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
        }
        return max(f[n - 1], g[n - 1]);
    }
};