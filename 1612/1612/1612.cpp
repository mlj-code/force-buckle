/**
 * Definition for a binary tree node.
 * struct Node {
 *     char val;
 *     Node *left;
 *     Node *right;
 *     Node() : val(' '), left(nullptr), right(nullptr) {}
 *     Node(char x) : val(x), left(nullptr), right(nullptr) {}
 *     Node(char x, Node *left, Node *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:

    void dfs(Node* node, vector<int>& v)
    {
        if (node == nullptr) return;
        v.push_back(node->val);
        dfs(node->left, v);
        dfs(node->right, v);
    }
    bool checkEquivalence(Node* root1, Node* root2)
    {
        vector<int> leaf1, leaf2;
        dfs(root1, leaf1);
        dfs(root2, leaf2);
        sort(leaf1.begin(), leaf1.end());
        sort(leaf2.begin(), leaf2.end());

        int n = leaf1.size();
        if (n != leaf2.size()) return false;
        for (int i = 0; i < n; i++) if (leaf1[i] != leaf2[i]) return false;

        return true;
    }
};