class Solution {
public:
    void cut(string& str, vector<int>& v)
    {
        string tmp;
        size_t begin = 0;
        size_t end = str.find('.', begin);
        while (end != string::npos)
        {
            tmp = str.substr(begin, end - begin);
            v.push_back(atoi(tmp.c_str()));
            begin = end + 1;
            end = str.find('.', begin);
        }
        tmp = str.substr(begin, end - begin);
        v.push_back(atoi(tmp.c_str()));
    }

    int cmp(string& version1, string& version2)
    {
        vector<int> v1, v2;
        cut(version1, v1);
        cut(version2, v2);

        int n = max(v1.size(), v2.size());
        v1.resize(n, 0), v2.resize(n, 0);//��ȱʧ�İ汾�Ų���Ϊ0
        for (int i = 0; i < n; i++)
        {
            if (v1[i] > v2[i]) return 1;
            else if (v1[i] < v2[i]) return -1;
        }
        return 0;
    }
    int compareVersion(string version1, string version2)
    {
        return cmp(version1, version2);
    }
};