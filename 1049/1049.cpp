class Solution {
public:
    int lastStoneWeightII(const vector<int>& stones)
    {
        //假设该组石头为a,b,c,d
        //最后的结果就是a,,b,c,d组合相加减,将加数分一堆,减数放一堆
        //二者之差就是结果,而二者越接近结果越小
        int sum = 0, n = stones.size();
        for (auto e : stones)sum += e;//先求出和
        int target = sum / 2;
        vector<bool> dp(target + 1, false);
        //背包为空的状态就是什么都不拿,合法
        dp[0] = true;

        for (int i = 1; i <= n; i++)
        {
            for (int j = target; j >= stones[i - 1]; j--)
            {
                dp[j] = dp[j] || dp[j - stones[i - 1]];
            }
        }
        int tmp = 0;
        for (int i = target; i >= 0; i--)
        {
            if (dp[i] == true)
            {
                tmp = i;
                break;
            }
        }
        return sum - tmp * 2;
    }
};