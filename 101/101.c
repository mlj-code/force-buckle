/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
bool Compare(struct TreeNode* left, struct TreeNode* right)
{
    if (left == NULL && right == NULL)
    {
        return true;
    }
    if (left == NULL || right == NULL)
    {
        return false;
    }
    else if (left->val != right->val)
    {
        return false;
    }

    return Compare(left->left, right->right) && Compare(left->right, right->left);
}


bool isSymmetric(struct TreeNode* root)
{
    if (root == NULL || root->left == NULL && root->right == NULL)
    {
        return true;
    }
    if (root->left == NULL || root->right == NULL)
    {
        return false;
    }
    return Compare(root->left, root->right);
}