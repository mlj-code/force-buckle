/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSymmetric(TreeNode* root)
    {
        deque<TreeNode*> dq, tmp;
        dq.push_front(root->left);
        dq.push_back(root->right);

        while (!dq.empty())
        {

            TreeNode* tmp1 = dq.front(); dq.pop_front();
            TreeNode* tmp2 = dq.back(); dq.pop_back();

            if (tmp1 != nullptr && tmp2 != nullptr)
            {
                if (tmp1->val != tmp2->val) return false;
                else
                {
                    dq.push_front(tmp1->left), dq.push_front(tmp1->right);
                    dq.push_back(tmp2->right), dq.push_back(tmp2->left);
                }
            }
            else if (tmp1 == nullptr && tmp2 == nullptr) continue;
            else return false;
        }

        return true;
    }
};