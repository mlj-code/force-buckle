class Solution {
public:
    bool isAnagram(string s, string t)
    {
        if (s.size() != t.size()) return false;
        unordered_map<char, int> hash;

        for (auto e : s)hash[e]++;

        for (int i = 0; i < t.size(); i++)
        {
            if (--hash[t[i]] < 0) return false;
        }

        return true;
    }
};