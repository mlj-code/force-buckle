class Solution {
public:
    void moveZeroes(vector<int>& nums)
    {
        int left = 0, right = 0;
        while (left < nums.size())
        {
            if (nums[left] == 0)break;
            else left++;
        }
        right = left + 1;
        while (right < nums.size())
        {
            if (nums[right] != 0)
            {
                nums[left] = nums[right];
                nums[right] = 0;
                left++;;
                right = left + 1;
            }
            else right++;
        }
    }
};