class Solution {
public:
    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2)
    {
        int n = nums1.size();
        vector<int> index(n), ret(n);
        for (int i = 0; i < n; i++)index[i] = i;
        //不能打乱num2的原始顺序,所以用索引排序
        sort(nums1.begin(), nums1.end());
        sort(index.begin(), index.end(), [&](int x, int y)
            {
                return nums2[x] < nums2[y];
            });
        int left = 0, right = n - 1;
        for (auto e : nums1)
        {
            //下等马比不过就去换上等马
            if (e > nums2[index[left]])
            {
                ret[index[left++]] = e;
            }
            else
            {
                ret[index[right--]] = e;
            }
        }

        return ret;
    }
};