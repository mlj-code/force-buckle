
#if 0//优化前
class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n)
    {
        int len = strs.size();
        vector<vector<vector<int>>>
            dp(len + 1, vector<vector<int>>(m + 1, vector<int>(n + 1, 0)));
        int a, b;
        for (int i = 1; i <= len; i++)
        {
            a = b = 0;
            //统计0,1的个数
            for (auto& e : strs[i - 1])
            {
                if (e == '0')a++;
                else b++;
            }

            for (int j = 0; j <= m; j++)
            {
                for (int k = 0; k <= n; k++)
                {
                    dp[i][j][k] = dp[i - 1][j][k];//不拿
                    if (j >= a && k >= b)//保证是由合法状态转换来的
                        dp[i][j][k] = max(dp[i][j][k], dp[i - 1][j - a][k - b] + 1);
                }
            }
        }
        return dp[len][m][n];
    }
};
#else if 1//优化后

class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n)
    {
        int len = strs.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        int a, b;
        for (int i = 1; i <= len; i++)
        {
            a = b = 0;
            //统计0,1的个数
            for (auto& e : strs[i - 1])
            {
                if (e == '0')a++;
                else b++;
            }
            for (int j = m; j >= a; j--)//从右往左填保证前面状态不会被污染
            {
                for (int k = n; k >= b; k--)
                {
                    dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
                }
            }
        }
        return dp[m][n];
    }
};

#endif