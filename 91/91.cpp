class Solution {
public:
    int numDecodings(string s)
    {
        if (s.size() == 1)
        {
            return s[0] == '0' ? 0 : 1;
        }
        int dp[101] = { 0 };
        if (s[0] != '0') dp[0] = 1;
        if (s[1] != '0') dp[1] += dp[0];
        if ((s[0] - '0') * 10 + s[1] - '0' < 27 && ((s[0] - '0') * 10 + s[1] - '0') > 9) dp[1]++;
        int i = 0;
        for (i = 2; i < s.size(); i++)
        {
            int tmp = (s[i - 1] - '0') * 10 + s[i] - '0';
            if (s[i] != '0') dp[i] += dp[i - 1];//自己解码成功则原本的解码数可以继承
            if (tmp < 27 && tmp>9) dp[i] += dp[i - 2];//合并解码成功则可额外多出dp[i-2]
            if (!dp[i]) return 0;
        }
        return dp[s.size() - 1];
    }
};