/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k)
    {
        if (k == 1)return head;
        ListNode* right = head, * left = head, * next, * cur, * ret, * pre = nullptr;
        stack<ListNode*> st;
        int n = k - 1;
        while (n--)
        {
            right = right->next;
        }
        ret = right;

        while (right != nullptr)
        {
            if (pre != nullptr) pre->next = right;//将此次的K歌节点与上次的连接起来
            pre = left;//更新
            next = right->next;//先保存下一部分
            cur = left;
            n = k - 1;
            //将需要逆序的K歌节点进栈
            while (n--)
            {
                st.push(cur);
                cur = cur->next;
            }
            left->next = next;

            while (!st.empty())
            {
                right->next = st.top();
                st.pop();
                right = right->next;
            }
            //更新下次的左右边界
            n = k - 2;
            left = next;
            if (left != nullptr)right = left->next;
            else return ret;

            while (right != nullptr && n--) right = right->next;
        }
        return ret;
    }
};