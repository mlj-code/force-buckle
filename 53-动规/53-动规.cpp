class Solution {
public:
    int maxSubArray(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> dp(n, 0);
        dp[0] = nums[0];
        int ret = nums[0];
        for (int i = 1; i < n; i++)
        {
            dp[i] = max(dp[i - 1], 0) + nums[i];
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};