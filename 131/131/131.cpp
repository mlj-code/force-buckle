class Solution {
public:
    vector<vector<string>> ret;
    unordered_map<int, unordered_map<int, string>> map;
    vector<string> tmp;
    bool dp[17][17];

    void dfs(string& s, int cur)
    {
        if (cur == s.size())
        {
            ret.push_back(tmp);
            return;
        }

        for (int i = cur; i < s.size(); i++)
        {
            if (dp[i][cur] != true)continue;

            tmp.push_back(map[i][cur]);
            dfs(s, i + 1);
            tmp.pop_back();
        }
    }

    vector<vector<string>> partition(string s)
    {
        int n = s.size();
        for (int i = 0; i < n; i++)//dp[i][j]表示i到j的子串是不是会文
        {
            dp[i][i] = true;
            map[i][i] = s.substr(i, 1);
            for (int j = i - 1; j >= 0; j--)
            {
                if (s[i] != s[j])
                {
                    dp[i][j] = false;
                    continue;
                }

                if (i - 1 == j)
                {
                    dp[i][j] = true;
                    map[i][j] = s.substr(j, i - j + 1);
                }
                else if (dp[i - 1][j + 1] == true)
                {
                    dp[i][j] = true;
                    map[i][j] = s.substr(j, i - j + 1);
                }
            }
        }

        dfs(s, 0);

        return ret;
    }
};