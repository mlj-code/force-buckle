//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        if (num1[0] == '0' || num2[0] == '0')
//        {
//            return "0";
//        }
//        int i = num2.size() - 1;
//        string tmp;
//        string mult1 = get(num1, num2[i--] - '0');
//        int count = 0;//计算时需下面的加数需往前挪几位
//        while (i + 1)//防止漏掉字符串的第零位
//        {
//            count++; int j = count;
//            string mult2 = get(num1, num2[i--] - '0');
//            while (mult2[0] != '0' && j--)
//            {
//                mult2 += '0';
//            }
//            tmp.clear();
//            tmp = add(mult1, mult2);
//            mult1.clear(); mult2.clear();
//            mult1 = tmp;
//        }
//        return mult1;
//    }
//private:
//    string get(string num, int x)
//    {
//        if (x == 0 || num[0] == '0')
//        {
//            return "0";
//        }
//        int len = num.size();
//        int i = len - 1;
//        int carry = 0;
//        string ret;
//        while (len--)
//        {
//            int digit = (num[i] - '0') * x + carry;
//            carry = digit / 10;
//            ret += digit % 10 + '0';
//            i--;
//        }
//        if (carry != 0)
//        {
//            ret += carry + '0';
//        }
//        return string(ret.rbegin(), ret.rend());
//    }
//    string add(string num1, string num2)
//    {
//        string::reverse_iterator pos1 = num1.rbegin();
//        string::reverse_iterator pos2 = num2.rbegin();
//        string ret;
//        char digit;
//        char carry = '0';
//        while (pos1 != num1.rend() && pos2 != num2.rend())
//        {
//            digit = (*pos1 + *pos2 + carry) - 3 * '0';
//            if (digit > 9)
//            {
//                digit = digit % 10 + '0';
//                carry = '1';
//            }
//            else
//            {
//                digit += '0';
//                carry = '0';
//            }
//            pos1++; pos2++;
//            ret += digit;
//        }
//        while (pos1 != num1.rend())
//        {
//            digit = *pos1 + carry - 2 * '0';
//            if (digit > 9)
//            {
//                digit = digit % 10 + '0';
//                carry = '1';
//            }
//            else
//            {
//                digit += '0';
//                carry = '0';
//            }
//            pos1++;
//            ret += digit;
//        }
//        while (pos2 != num2.rend())
//        {
//            digit = *pos2 + carry - 2 * '0';
//            if (digit > 9)
//            {
//                digit = digit % 10 + '0';
//                carry = '1';
//            }
//            else
//            {
//                digit += '0';
//                carry = '0';
//            }
//            ret += digit;
//            pos2++;
//        }
//        if (carry == '1')
//        {
//            ret += '1';
//        }
//        return string(ret.rbegin(), ret.rend());
//    }
//};


class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1[0] == '0' || num2[0] == '0') {
            return "0";
        }
        string s1;
        int n1 = num1.size(), n2 = num2.size();
        vector<int> num(n1 + n2, 0);
        for (int i = n2 - 1; i >= 0; i--) {
            int x = num2[i] - '0';
            for (int j = n1 - 1; j >= 0; j--) {
                int y = num1[j] - '0';
                num[i + j + 1] += x * y;
            }
        }

        for (int i = n1 + n2 - 1; i > 0; i--) {
            int x = num[i] / 10;
            num[i] %= 10;
            num[i - 1] += x;
        }

        string res;
        bool flag = false;
        for (int i = 0; i < num.size(); i++) {
            if (num[i] != 0) //找到有效最高位
            {
                flag = true;
            }
            if (flag)
            {
                res.push_back(num[i] + '0');
            }
        }

        return res;
    }
};