class Solution {
public:
    int findLengthOfLCIS(vector<int>& nums)
    {
        int ret = 1, l = 0, r = 0;
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] > nums[r])
            {
                r++;
            }
            else
            {
                ret = max(ret, r - l + 1);
                l = r = i;
            }
        }
        ret = max(ret, r - l + 1);
        return ret;
    }
};