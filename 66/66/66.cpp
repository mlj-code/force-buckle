class Solution {
public:
    vector<int> plusOne(vector<int>& digits)
    {
        vector<int> ret = digits;
        ret.back()++;
        for (int i = ret.size() - 1; i >= 0; i--)
        {
            if (ret[i] > 9)
            {
                int tmp = ret[i];
                ret[i] = tmp % 10;
                if (i - 1 >= 0)
                {
                    ret[i - 1]++;
                }
                else
                {
                    ret.insert(ret.begin(), 1);
                }
            }
        }
        return ret;
    }
};