class Solution {
public:
    int minCost(vector<vector<int>>& costs)
    {
        int n = costs.size();
        int r[101];
        int g[101];
        int b[101];//表示当前房子刷当前颜色的最少花费
        r[0] = costs[0][0];
        g[0] = costs[0][1];
        b[0] = costs[0][2];
        for (int i = 1; i < n; i++)
        {
            r[i] = min(g[i - 1], b[i - 1]) + costs[i][0];
            g[i] = min(r[i - 1], b[i - 1]) + costs[i][1];
            b[i] = min(r[i - 1], g[i - 1]) + costs[i][2];
        }
        return min(min(r[n - 1], g[n - 1]), b[n - 1]);
    }
};