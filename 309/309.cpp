class Solution {
public:
    int maxProfit(vector<int>& prices)
    {
        int f[5001];//买入状态的最大利润
        int g[5001];//冷冻
        int x[5001];//可交易
        f[0] = -1 * prices[0];
        g[0] = 0;
        x[0] = 0;
        int i = 1;
        for (; i < prices.size(); i++)
        {
            f[i] = max(x[i - 1] - prices[i], f[i - 1]);
            g[i] = f[i - 1] + prices[i];
            x[i] = max(x[i - 1], g[i - 1]);
        }
        return max(g[i - 1], x[i - 1]);
    }
};