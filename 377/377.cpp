class Solution {
public:
    int combinationSum4(vector<int>& nums, int target)
    {
        vector<int> dp(target + 1, 0);
        dp[0] = 1;
        for (int i = 1; i <= target; i++)
        {
            //可从每一个位置变化来
            for (auto x : nums)
            {
                //如果二者相加大于32位整数,说明最终答案不会用到该状态
                //因为题目明确不超出32位
                if (i >= x && dp[i] < INT_MAX - dp[i - x])
                {
                    dp[i] += dp[i - x];
                }
            }
        }
        return dp[target];
    }
};