class LRUCache {
public:
    struct node
    {
        int _key;
        int _val;
        node* _next;
        node* _pre;
        node(int key, int val, node* pre = nullptr, node* next = nullptr)
            :_key(key), _val(val), _pre(pre), _next(next)
        {}
    };
    struct list
    {
        node* _head;

        list()
        {
            _head = new node(0, 0);
            _head->_next = _head, _head->_pre = _head;
        }

        node* push_back(int key, int val)
        {
            node* tmp = new node(key, val);
            update(tmp);
            return tmp;
        }

        void update(node* n)//将节点调整到最后
        {
            n->_next = _head, n->_pre = _head->_pre;
            _head->_pre->_next = n;
            _head->_pre = n;
        }

        node* pop(node* tmp)//只断开要删除节点并连接其前后
        {

            tmp->_pre->_next = tmp->_next;
            tmp->_next->_pre = tmp->_pre;
            return tmp;
        }

    };

    list _list;
    int _cap;
    unordered_map<int, node*> _map;

    LRUCache(int capacity)
        :_cap(capacity)
    {
        _map.reserve(capacity);
    }

    int get(int key)
    {
        if (_map.count(key) != 0)//获取后将节点更行到最新位置
        {
            _list.pop(_map[key]);
            _list.update(_map[key]);
            return _map[key]->_val;
        }
        else return -1;
    }

    void put(int key, int value)
    {

        if (_cap != _map.size())
        {
            if (_map.count(key) == 0)//节点没出现过,直接插入
            {
                node* tmp = _list.push_back(key, value);
                _map[key] = tmp;
            }
            else//出现过,覆盖V值
            {
                _map[key]->_val = value;
                _list.pop(_map[key]);
                _list.update(_map[key]);
            }
        }
        else//节点满了
        {
            if (_map.count(key) == 0)//节点没出现过,需要覆盖
            {
                node* tmp = _list.pop(_list._head->_next);//断开最旧的节点连接
                _map.erase(tmp->_key);

                tmp->_key = key, tmp->_val = value;
                _map[key] = tmp;
                _list.update(tmp);//将节点调整到最后
            }
            else//节点出现过,只需要覆盖V值
            {
                _map[key]->_val = value;
                _list.pop(_map[key]);//断开原本的连接
                _list.update(_map[key]);//将节点更新到最后
            }
        }
    }
};
