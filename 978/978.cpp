class Solution {
public:
    char getChar(int left, int right)
    {
        if (left < right)return '<';
        else if (left > right)return '>';
        else return '=';
    }
    int maxTurbulenceSize(vector<int>& arr)
    {
        int n = arr.size();
        if (n == 1)return 1;
        int dp[40000] = { 0 };
        dp[0] = 1;
        if (arr[0] != arr[1])dp[1] = 2;
        else dp[1] = 1;
        int ret = dp[1];
        char last = getChar(arr[0], arr[1]);
        char cur;
        for (int i = 2; i < n; i++)
        {
            cur = getChar(arr[i - 1], arr[i]);
            if (cur == '=')dp[i] = 1;
            else if (cur != last) dp[i] = dp[i - 1] + 1;
            else dp[i] = 2;
            last = cur;
            if (dp[i] > ret) ret = dp[i];
        }
        return ret;
    }
};