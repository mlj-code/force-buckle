class Solution {
public:
    int maxNumberOfAlloys(int n, int k, int budget, vector<vector<int>>& composition, vector<int>& stock, vector<int>& cost)
    {
        int ret = 0;
        int left = 0, right = 2e8;

        while (left <= right)//二者相等也要进去判断是否符合条件然后更新答案
        {
            int mid = (left + right) >> 1;
            bool flag = false;//是否能继续的标志位
            for (int i = 0; i < k; i++)//遍历每台机器的生产方案
            {
                long long spend = 0;
                for (int j = 0; j < n; j++)
                {
                    spend += max((long long)(composition[i][j]) * mid - stock[j], 0LL) * cost[j];
                }
                if (spend <= budget)//只要有一个机器能符合方案就继续
                {
                    flag = true;
                    break;
                }
            }
            if (flag == true)
            {
                ret = mid;
                left = mid + 1;
            }
            else
            {
                right = mid - 1;
            }
        }
        return ret;
    }
};