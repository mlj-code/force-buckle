#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)


typedef struct
{
    int* stk;
    int* stk2;
    int size;
    int capicity;
} MinStack;

/** initialize your data structure here. */

MinStack* minStackCreate()
{
    MinStack* ret = (MinStack*)malloc(sizeof(MinStack));
    ret->stk = (int*)malloc(sizeof(int) * 20000);
    ret->stk2 = (int*)malloc(sizeof(int) * 20000);
    ret->size = 0;
    ret->capicity = 20000;
    return ret;
}

void minStackPush(MinStack* obj, int x)
{
    obj->stk[obj->size] = x;
    obj->stk2[0] = obj->stk[0];
    if (obj->size > 0)
    {
        if ((obj->stk2[obj->size - 1]) > x)
        {
            obj->stk2[obj->size] = x;
        }
        else
        {
            obj->stk2[obj->size] = obj->stk2[obj->size - 1];
        }
    }
    obj->size++;
}

void minStackPop(MinStack* obj)
{
    obj->size--;
}

int minStackTop(MinStack* obj)
{
    return obj->stk[obj->size - 1];
}

int minStackMin(MinStack* obj)
{
    return obj->stk2[obj->size - 1];
}

void minStackFree(MinStack* obj)
{
    free(obj->stk);
    free(obj);
}

/**
 * Your MinStack struct will be instantiated and called as such:
 * MinStack* obj = minStackCreate();
 * minStackPush(obj, x);

 * minStackPop(obj);

 * int param_3 = minStackTop(obj);

 * int param_4 = minStackMin(obj);

 * minStackFree(obj);
*/
