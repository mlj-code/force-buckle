class Solution {
public:
    string removeKdigits(string num, int k)
    {
        string tmp;
        for (auto ch : num)
        {
            //单调栈,如果前面的数更大则采取较小的值来填充高位
            while (!tmp.empty() && tmp.back() > ch && k > 0)
            {
                tmp.pop_back();
                k--;
            }

            tmp += ch;
        }

        while (k > 0)//如果还没拿够则从低位开始拿,因为此时数字已经符合从高位到低位依次递增
        {
            tmp.pop_back();
            k--;
        }

        int start = 0;
        //去除前导0
        while (start < tmp.size() && tmp[start] == '0') start++;
        tmp = tmp.substr(start, -1);
        return tmp.empty() ? "0" : tmp;
    }
};