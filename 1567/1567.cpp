class Solution {
public:
    int getMaxLen(vector<int>& nums)
    {
        int f[500000] = { 0 };//表示乘积为正
        int g[500000] = { 0 };//乘积为负最长
        if (nums[0] > 0)
        {
            f[0] = 1;
            g[0] = 0;
        }
        else if (nums[0] == 0)
        {
            f[0] = g[0] = 0;
        }
        else
        {
            f[0] = 0;
            g[0] = 1;
        }
        int ret = f[0];
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;//如果前面没有负数且nums[i]也是正数则构不成乘积负数
            }
            else if (nums[i] == 0)
            {
                cout << i << ' ';
                f[i] = g[i] = 0;
            }
            else
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;//如果前面没有负数且num[i]也是负数则无法构成乘积正数
                g[i] = f[i - 1] + 1;
            }
            if (f[i] > ret)ret = f[i];
        }
        return ret;
    }
};