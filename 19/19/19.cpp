/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n)
    {
        ListNode* slow = head, * fast = head;
        n++;//确保快指针到尾时,慢指针指向的是要要删除节点的前一个
        while (fast != nullptr && n--)
        {
            fast = fast->next;
        }
        //如果提前结束说明要删除的是头节点
        if (fast == nullptr && n != 0)
        {
            slow = head->next;
            delete head;
            return slow;
        }

        while (fast != nullptr)
        {
            fast = fast->next;
            slow = slow->next;
        }
        fast = slow->next;
        slow->next = slow->next->next;
        delete fast;
        return head;
    }
};