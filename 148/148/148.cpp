class Solution {
public:
    ListNode* sortList(ListNode* head)
    {
        if (head == nullptr || head->next == nullptr)return head;

        ListNode* dumHead = new ListNode(0, head);//哨兵头节点

        int len = 0;
        ListNode* cur = head;
        while (cur != nullptr)len++, cur = cur->next;//统计链表长度

        for (int subLen = 1; subLen < len; subLen <<= 1)//将原链表划分成子链表归并排序
        {
            ListNode* pre = dumHead;//用于将链表重新串联起来
            cur = dumHead->next;//遍历链表,将每个子链表排序
            while (cur != nullptr)
            {
                ListNode* head1, * head2, * next;
                head1 = cur;
                //遍历到第一个链表尾,尾的下一个节点就是第二个链表的头
                for (int i = 1; i < subLen && cur != nullptr; i++) cur = cur->next;
                if (cur == nullptr) break;
                head2 = cur->next;
                cur->next = nullptr;//将第一个链表尾置空
                cur = head2;
                //取到第二个链表尾
                for (int i = 1; i < subLen && cur != nullptr; i++) cur = cur->next;

                //方便连接后面的链表,如果第二个子链表提前结束则尾为空
                //否则需要保存后一段链表的头,方便连接并将划分的第二个链表尾置空
                next = nullptr;
                if (cur != nullptr)
                {
                    next = cur->next;
                    cur->next = nullptr;
                }
                ListNode* tail;
                ListNode* sorted = merge(head1, head2, tail);//合并有序子链表
                //将分开的后段子链表重新连接
                tail->next = next;//将链表重新连接
                pre->next = sorted;
                pre = tail;//本次的尾作为先导节点连接下次排好序的链表
                cur = next;

            }
        }
        return dumHead->next;
    }

    ListNode* merge(ListNode* head1, ListNode* head2, ListNode*& tail)
    {
        if (head1 == nullptr)
        {
            tail = head2;
            while (tail->next != nullptr) tail = tail->next;
            return head2;
        }

        if (head2 == nullptr)
        {
            tail = head1;
            while (tail->next != nullptr) tail = tail->next;
            return head1;
        }
        ListNode* ret = new ListNode(0);
        ListNode* cur = ret;
        while (head1 != nullptr && head2 != nullptr)
        {
            if (head1->val < head2->val)
            {
                cur->next = head1;
                cur = cur->next;
                head1 = head1->next;
            }
            else
            {
                cur->next = head2;
                cur = cur->next;
                head2 = head2->next;
            }
            tail = cur;
        }

        if (head1 != nullptr)
        {
            cur->next = head1;
            while (cur->next != nullptr) cur = cur->next;
            tail = cur;
        }
        else if (head2 != nullptr)
        {
            cur->next = head2;
            while (cur->next != nullptr) cur = cur->next;
            tail = cur;
        }

        ListNode* tmp = ret;
        ret = ret->next;
        delete tmp;
        return ret;
    }
};