class Solution {
public:
    int searchInsert(vector<int>& nums, int target)
    {
        if (target > nums[nums.size() - 1]) return nums.size();
        int left = 0; int right = nums.size() - 1;
        while (left < right)
        {
            int mid = (left + right) / 2;
            if (nums[mid] == target) return mid;
            else if (nums[mid] < target) left = mid + 1;
            else right = mid;
        }
        return right;
    }
};