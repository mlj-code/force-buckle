class Solution {
public:
    int firstMissingPositive(vector<int>& nums)
    {
        const int n = nums.size();
        bitset<500000> set;
        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] > 0 && nums[i] <= n)set[nums[i]] = true;
        }
        for (int i = 1; i < n + 1; i++)
        {
            if (set[i] == false)
                return i;
        }
        return n + 1;
    }
};