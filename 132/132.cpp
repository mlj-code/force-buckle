class Solution {
public:
    int minCut(string s)
    {
        int n = s.size();
        vector<vector<bool>> is_palin(n, vector<bool>(n));
        for (int i = 1; i < n; i++)
        {
            for (int j = i; j >= 0; j--)
            {
                if (s[i] == s[j])
                {
                    is_palin[i][j] = j + 1 < i ? is_palin[i - 1][j + 1] : true;
                }
            }
        }

        vector<int> dp(n, 0);
        for (int i = 1; i < n; i++)
        {
            if (is_palin[i][0] != true)
            {
                int tmp = dp[i - 1];
                for (int j = 0; j < i; j++)
                {
                    if (is_palin[i][j] == true)
                    {
                        tmp = min(dp[j - 1], tmp);
                    }
                    dp[i] = tmp + 1;
                }
            }
        }
        return dp[n - 1];
    }
};