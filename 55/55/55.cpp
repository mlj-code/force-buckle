class Solution {
public:
    bool canJump(vector<int>& nums)
    {
        int n = nums.size();
        int end = 0;
        for (int i = 0; i <= end; i++)
        {
            end = max(end, nums[i] + i);//不断更新最远能到达的地方

            if (end >= n - 1) return true;//最远能到达n-1下标就赢了
        }

        return false;
    }
};