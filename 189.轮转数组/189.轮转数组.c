#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include<stdio.h>

void rotate(int* nums, int numsSize, int k)
{
    int tmp[4];
    k = k % numsSize;
    if (k == 0)
        return;
    int i = 0;
    int j = 0;
    for (i = k, j = 0; i>0; i--, j++)
    {
        tmp[j] = nums[numsSize -i];
    }
    for (i = 0; j < numsSize; i++, j++)
    {
        tmp[j] = nums[i];
    }

    for (i = 0; i < numsSize; i++)
    {
        nums[i] = tmp[i];
    }

}

int main()
{
    int nums[] = { 1,2,3,4 };
    int numsSize = 4;
    rotate(nums, numsSize, 2);
	return 0;
}

