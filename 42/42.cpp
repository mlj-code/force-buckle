#if 0
class Solution {
public:
    //计算雨水,flag为真则左低右高
    int cal(const vector<int>& height, int slow, int fast, bool flag)
    {
        int ret = 0, cur;
        if (flag == true)
        {
            cur = slow + 1;
            while (cur < fast)
            {
                ret += height[slow] - height[cur];
                cur++;
            }
            return ret;
        }
        else
        {
            cur = fast - 1;
            while (cur > slow)
            {
                ret += height[fast] - height[cur];
                cur++;
            }
            return ret;
        }
    }


    int trap(const vector<int>& height)
    {
        int n = height.size();
        int slow = 0, fast = 0, ret = 0, tmp = 0;
        while (fast < n)
        {
            while (slow < n && height[slow] == 0)
            {
                slow++;
            }
            fast = slow + 1;
            tmp = fast;
            while (fast < n && height[fast] < height[slow])
            {
                //如果一直没找到比慢指针高的快指针,那tmp将作为右池子的右墙
                if (height[fast] > height[tmp]) tmp = fast;
                fast++;
            }
            if (fast == n)//表示找不到比慢指针高的墙,雨水计算从左边开始
            {
                fast = tmp;
                ret += cal(height, slow, fast, false);
            }
            else
            {
                ret += cal(height, slow, fast, true);
            }
            slow = fast;
        }
        return ret;
    }
};

//动态规划
# else if 1

class Solution {
public:
    int trap(vector<int>& height)
    {
        int n = height.size();
        vector<int> left_max(n, 0), right_max(n, 0);
        left_max[0] = height[0];
        right_max[n - 1] = height[n - 1];
        //先各把池子的限制假设为左边和右边,最后取值假设两种情况下雨水量的最小值
        for (int i = 1; i < n; i++)
        {
            left_max[i] = max(height[i], left_max[i - 1]);
            right_max[n - 1 - i] = max(right_max[n - i], height[n - 1 - i]);
        }
        int ret = 0;
        for (int i = 0; i < n; i++)
        {
            ret += min(left_max[i], right_max[i]) - height[i];
        }
        return ret;
    }
};

#endif