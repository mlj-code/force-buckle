#include"˫ջʵ�ֶ���.h"


stack* StackCreate(int capacity)
{
	stack* ret = (stack*)malloc(sizeof(stack));
	ret->capacity = capacity;
	ret->size = 0;
	ret->stk = (int*)malloc(capacity);
	return ret;
}


CQueue* cQueueCreate()
{
	CQueue* ret = (CQueue*)malloc(sizeof(CQueue));
	ret->InStack = StackCreate(10000);
	ret->OutStack = StackCreate(10000);

	return ret;
}


int is_empty(stack* st)
{
	if (st->size == 0)
		return 1;
	else
		return 0;
}

void StackPush(stack* st, int value)
{
	st->stk[st->size] = value;
	st->size++;
}

int StackTop(stack* st)
{
	st->size--;
	return st->stk[st->size];
}

void InStackToOutStack(stack* in, stack* out)
{
	while (!is_empty(in))
	{
		StackPush(out, StackTop(in));
	}
}

void cQueueAppendTail(CQueue* obj, int value)
{
	StackPush(obj->InStack, value);
}

int cQueueDeleteHead(CQueue* obj)
{
	if (is_empty(obj->OutStack))
	{
		if (is_empty(obj->InStack))
		{
			return -1;
		}
		InStackToOutStack(obj->InStack, obj->OutStack);
		
	}

	int ret = StackTop(obj->OutStack);
	return ret;
}

void StackFree(stack* st)
{
	free(st->stk);
	free(st);
}

void cQueueFree(CQueue* obj)
{
	free(obj->InStack);
	free(obj->OutStack);
}

/**
 * Your CQueue struct will be instantiated and called as such:
 * CQueue* obj = cQueueCreate();
 * cQueueAppendTail(obj, value);

 * int param_2 = cQueueDeleteHead(obj);

 * cQueueFree(obj);
*/