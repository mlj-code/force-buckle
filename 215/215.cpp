class Solution {
public:

    void swap(int* left, int* right)
    {
        int tmp = *left;
        *left = *right;
        *right = tmp;
    }
    void getmid(int* arr, int left, int right)
    {
        int mid = (right + left) / 2;
        if (arr[left] > arr[right])
        {
            mid = arr[right] > arr[mid] ? right : mid;
        }
        else
        {
            mid = arr[left] > arr[mid] ? left : mid;
        }
        swap(&arr[left], &arr[mid]);
    }
    void qsort(int* arr, int left, int right, int k)
    {
        getmid(arr, left, right);
        int keyi = left; int begin = left; int end = right;
        while (left < right)
        {
            while (left < right && arr[right] >= arr[keyi])//�ұ���С
            {
                right--;
            }
            while (left < right && arr[left] <= arr[keyi])
            {
                left++;
            }
            swap(&arr[left], &arr[right]);
        }
        swap(&arr[keyi], &arr[left]);
        keyi = left;
        if (keyi == k)
        {
            return;
        }
        else if (keyi > k)//��ֵ����Ŀ��λ����ݹ�����
        {
            qsort(arr, begin, keyi, k);
        }
        else
        {
            qsort(arr, keyi + 1, end, k);
        }
    }

    int findKthLargest(vector<int>& nums, int k)
    {
        qsort(nums.data(), 0, nums.size() - 1, nums.size() - k);
        return nums[nums.size() - k];
    }
};