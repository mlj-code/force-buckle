/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
#if 0 //优化前,递归
class Solution {
public:
    ListNode* mregeSort(ListNode* l1, ListNode* l2)
    {
        if (l1 == nullptr)return l2;
        else if (l2 == nullptr) return l1;

        ListNode* ret, * tmp;
        ret = tmp = new ListNode(0);

        while (l1 != nullptr && l2 != nullptr)
        {
            if (l1->val < l2->val)
            {
                tmp->next = l1;
                l1 = l1->next;
            }
            else  tmp->next = l2, l2 = l2->next;

            tmp = tmp->next;
        }

        if (l1 != nullptr) tmp->next = l1;
        else tmp->next = l2;

        tmp = ret;
        ret = ret->next;
        delete tmp;
        return ret;
    }

    ListNode* mergeKLists(vector<ListNode*>& lists)
    {
        int n = lists.size();
        if (n == 0)return nullptr;
        ListNode* ret = lists[0];
        for (int i = 1; i < n; i++)
        {
            ret = mregeSort(ret, lists[i]);
        }
        return ret;
    }
};

#elseif 0//优化前非递归
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists)
    {
        int n = lists.size();
        if (n == 0)return nullptr;
        ListNode* ret = lists[0];
        ListNode* l1, * l2, * tmp;
        ListNode* head = new ListNode(0, ret);
        for (int i = 1; i < n; i++)
        {
            l1 = ret, l2 = lists[i], tmp = head;
            //逐步合并两个链表
            while (l1 != nullptr && l2 != nullptr)
            {
                if (l1->val < l2->val)
                {
                    tmp->next = l1;
                    l1 = l1->next;
                }
                else  tmp->next = l2, l2 = l2->next;

                tmp = tmp->next;
            }
            if (l1 != nullptr) tmp->next = l1;
            else tmp->next = l2;

            ret = head->next;
        }
        return head->next;
    }
};

#else //优化后

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists)
    {
        auto cmp = [](ListNode* l1, ListNode* l2) {
            return l1->val > l2->val;
            };

        ListNode* head, * cur, * tmp;
        head = cur = new ListNode(0);
        priority_queue<ListNode*, vector<ListNode*>, decltype(cmp)> q;
        for (auto l : lists)
        {
            if (l != nullptr)
                q.push(l);
        }

        while (!q.empty())
        {
            tmp = q.top();
            cur->next = tmp, cur = cur->next, tmp = tmp->next;
            q.pop();
            if (tmp != nullptr) q.push(tmp);
        }
        return head->next;

    }
};

#endif