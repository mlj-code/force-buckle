class Solution {
public:
    int numTilePossibilities(string tiles) {
        unordered_map<char, int> count;
        set<char> tile;
        int n = tiles.length();
        for (char c : tiles) {
            count[c]++;
        }
        return dfs(count) - 1;
    }

    int dfs(unordered_map<char, int>& count) {

        int res = 1;
        for (auto& [ch, cnt] : count)
        {
            if (cnt > 0)
            {
                cnt--;
                res += dfs(count);
                cnt++;
            }
        }
        return res;
    }
};
