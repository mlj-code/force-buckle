class Solution {
public:
    int lenLongestFibSubseq(vector<int>& arr)
    {
        int ret = 2;
        int n = arr.size();
        vector<vector<int>> dp(n, vector<int>(n, 2));
        unordered_map<int, int> m;
        for (int i = 0; i < n; i++)m[arr[i]] = i;
        for (int i = 2; i < n; i++)
        {
            for (int j = 1; j < i; j++)
            {
                int first = arr[i] - arr[j];
                if (first < arr[j] && m.count(first) != 0)
                {
                    dp[i][j] = dp[j][m[first]] + 1;
                }
                ret = max(ret, dp[i][j]);
            }
        }
        return ret >= 3 ? ret : 0;
    }
};