class Solution {
public:
    string longestCommonPrefix(vector<string>& strs)
    {
        if (strs.size() == 1) return strs[0];
        int left = 0, right = strs[0].size();
        string ret;
        while (left <= right)
        {
            int mid = (left + right) >> 1;
            bool flag = true;
            string tmp = strs[0].substr(0, mid);

            for (int i = 1; i < strs.size(); i++)
            {
                if (strs[i].size() < mid)
                {
                    right = strs[i].size();
                    flag = false;
                    break;
                }

                for (int j = 0; j < mid; j++)//直接比较比提取子串比较速度要快
                {
                    if (tmp[j] != strs[i][j])
                    {
                        right--;
                        flag = false;
                        break;
                    }
                }
            }

            if (flag == true)
            {
                ret = tmp;
                left = mid + 1;
            }
        }

        return ret;
    }
};