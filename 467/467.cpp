class Solution {
public:
    int findSubstringInWraproundString(string s)
    {
        int n = s.size();
        vector<int> dp(n, 1);
        for (int i = 1; i < n; i++)
        {
            if (s[i] - 1 == s[i - 1] || (s[i] == 'a' && s[i - 1] == 'z'))
                dp[i] += dp[i - 1];
        }
        int hase[26] = { 0 };
        for (int i = 0; i < n; i++)
        {
            hase[s[i] - 'a'] = max(hase[s[i] - 'a'], dp[i]);
        }
        int ret = 0;
        for (auto e : hase)ret += e;
        return ret;
    }
};