class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int h)
    {
        int left = 1, right;
        int n = piles.size();
        for (auto& x : piles)right = max(right, x);
        while (left < right)
        {
            int mid = (left + right) >> 1;
            int tmph = h;
            int i = 0;
            for (; i < n && tmph>0; i++)
            {

                int tmp = piles[i];
                tmph -= (tmp / mid);
                if (tmp % mid != 0)tmph--;
            }
            if (i == n && tmph >= 0)
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }
        }
        return left;
    }
};