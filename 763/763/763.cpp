class Solution {
public:
    vector<int> partitionLabels(string s)
    {
        vector<int> ret;
        int last[26];
        int n = s.size();
        for (int i = 0; i < n; i++)last[s[i] - 'a'] = i;//更新每个字母的最远下标

        int start = 0, end = 0;
        for (int i = 0; i < n; i++)
        {
            end = max(end, last[s[i] - 'a']);//不断更新当前字母最后一次出现的最后出现位置
            if (i == end)//如果当前已经到达最后出现位置,则可分割成一段
            {
                ret.push_back(i - start + 1);
                start = i + 1;
            }
        }

        return ret;
    }
};