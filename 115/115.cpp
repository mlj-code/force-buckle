class Solution {
public:
    int numDistinct(string s, string t)
    {
        int n = s.size();
        int m = t.size();
        //dp[i][j]表示在s[0,i]的区间内有多少个t[0,j]子序列
        vector<vector<int>> dp(n, vector<int>(m, 0));
        if (s[0] == t[0])dp[0][0] = 1;
        for (int i = 1; i < n; i++)
        {
            if (s[i] == t[0])
            {
                dp[i][0] = 1 + dp[i - 1][0];
            }
            else
            {
                dp[i][0] = dp[i - 1][0];
            }
        }
        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)//遍历目标数组
            {
                if (s[j] == t[i])
                {
                    dp[j][i] = (dp[j - 1][i - 1] + dp[j - 1][i]) % ((int)pow(10, 9) + 7);
                }
                else
                {
                    dp[j][i] = dp[j - 1][i];
                }
            }
        }
        return dp[n - 1][m - 1];
    }
};