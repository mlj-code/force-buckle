#include<vector>
#include<iostream>
using namespace std;

class Solution {
public:
    vector<vector<int>> generate(int numRows)
    {
        vector<vector<int>> vv;
        vector<int> v;
        v.reserve(30);
        v.push_back(1);
        vv.push_back(vector<int>(v.begin(), v.end()));
        v.clear();
        if (numRows == 1)
        {
            return vv;
        }
        for (int  i = 2; i <= numRows; i++)
        {
            v.push_back(1);
            vector<int> a = vv[i - 2];
            for (int  j = 1; j <= i - 2; j++)
            {
                //v[j] = a[j - 1]+a[j];//不能,会越界
                v.push_back(a[j] + a[j - 1]);
            }
            v.push_back(1);
            vv.push_back(vector<int>(v.begin(), v.end()));
            v.clear();
            a.clear();
        }
        return vv;
    }
};


int main()
{
    Solution T;
    vector<vector<int>> vv=T.generate(5);

    for (auto& a : vv)
    {
        for (auto& b : a)
        {
            cout << b << ' ';
        }
        cout << endl;
    }


    return 0;
}