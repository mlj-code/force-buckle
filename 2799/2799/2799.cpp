class Solution {
public:
    int countCompleteSubarrays(vector<int>& nums)
    {
        unordered_map<int, int> map;
        unordered_map<int, int> cnt;
        int n = nums.size();
        int ret = 0;
        for (int i = 0; i < n; i++)map[nums[i]]++;

        for (int i = 0; i < n; i++)
        {
            for (int j = i; j < n; j++)
            {
                cnt[nums[j]]++;
                if (cnt.size() == map.size())
                {
                    ret += n - j;
                    break;
                }
            }
            if (cnt.size() != map.size())
            {
                break;
            }
            else cnt.clear();
        }
        return ret;
    }
};