class Solution {
public:
    vector<string> ret;
    string tmp;
    int len;

    void fds(int x, int y)
    {
        if (x == len && y == len)
        {
            ret.push_back(tmp);
            return;
        }
        //只能向下走或向右走,向下走是'(',向右走是')'
        //坐标系是纵向x向下,横向y向右,走的路线不能超过对角线
        if (x + 1 <= len && y <= len && x + 1 >= y)
        {

            tmp.push_back('(');
            fds(x + 1, y);
            tmp.pop_back();
        }

        if (y + 1 <= len && x <= len && x >= y + 1)
        {
            tmp.push_back(')');
            fds(x, y + 1);
            tmp.pop_back();
        }
    }
    vector<string> generateParenthesis(int n)
    {
        len = n;
        tmp.push_back('(');
        fds(1, 0);
        return ret;
    }
};