class Solution {
public:
    string str;
    int ptr = 0;
    int getcnt()
    {
        int ret = 0;
        while ((str[ptr] - '0') < 10 && (str[ptr] - '0') >= 0)
        {
            ret = ret * 10 + str[ptr++] - '0';
        }
        return ret;
    }

    string getstr()
    {
        if (ptr >= str.size() || str[ptr] == ']') return "";


        string tmp, sub;
        int cnt = 1;
        while (ptr < str.size())
        {
            if (str[ptr] >= 'a' && str[ptr] <= 'z')//正常字符直接处理
            {
                tmp += str[ptr++];
                continue;
            }

            if (str[ptr] == ']')//如果到这一步就需要返回
            {
                ptr++;
                return tmp;
            }

            if ((str[ptr] - '0') < 10 && (str[ptr] - '0') > 0)//是数字,获取数字
            {
                cnt = getcnt();//数字后紧接着就是'['
                ptr++;//跳过'['
            }
            sub = getstr();
            while (cnt--)tmp += sub;
        }

        return tmp;
    }

    string decodeString(string s)
    {
        str = s;
        return getstr();
    }
};