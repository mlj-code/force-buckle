class Solution {
public:
    int n;
    vector<vector<int>> next;
    int ret = 0;

    int dfs(int root, string& s)
    {
        int l = 0, r = 0;//存放最长和次长
        for (auto& child : next[root])
        {
            int tmp = dfs(child, s);
            if (s[root] != s[child])
            {
                if (tmp > r) l = r, r = tmp;
                else if (tmp > l) l = tmp;
            }
        }
        ret = max(ret, l + r + 1);

        return r + 1;
    }
    int longestPath(vector<int>& parent, string s)
    {
        n = parent.size();
        next = vector<vector<int>>(n);
        for (int i = 0; i < n; i++) if (parent[i] != -1)next[parent[i]].push_back(i);

        dfs(0, s);

        return ret;
    }
};