class Solution {
public:
    string longestPalindrome(string s)
    {
        int n = s.size();
        int left = 0;
        int len = 1;
        vector<vector<int>> dp(n, vector<int>(n, 0));
        for (int i = 0; i < n; i++)
        {
            dp[i][i] = 1;
            for (int j = i - 1; j >= 0; j--)
            {
                if (s[i] == s[j])
                {
                    if (j + 1 == i)
                    {
                        dp[i][j] = 2;
                    }
                    else if (dp[i - 1][j + 1] != 0)
                    {
                        dp[i][j] = dp[i - 1][j + 1] + 2;
                    }


                    if (dp[i][j] > len)
                    {
                        len = dp[i][j];
                        left = j;
                    }
                }
            }
        }
        return s.substr(left, len);
    }
};