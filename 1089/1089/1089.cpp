class Solution {
public:
    void duplicateZeros(vector<int>& arr)
    {
        int i = 0, j = 0;
        int n = arr.size();
        while (j < n)
        {
            if (arr[i] == 0)j++;
            i++, j++;
        }
        i--, j--;
        while (i != j)
        {
            if (j < n)arr[j] = arr[i];
            if (arr[i] == 0 && --j >= 0)arr[j] = 0;
            i--, j--;
        }
    }
};