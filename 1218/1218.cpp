class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference)
    {
        unordered_map<int, int> hash;
        hash[arr[0]] = 1;
        int ret = 1;
        for (int i = 1; i < arr.size(); i++)
        {
            hash[arr[i]] = hash[arr[i] - difference] + 1;//如果不存在则返回0
            ret = max(ret, hash[arr[i]]);
        }
        return ret;

    }
};