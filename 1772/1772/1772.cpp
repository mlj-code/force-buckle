class Solution {
public:
    std::vector<std::string> sortFeatures(std::vector<std::string>& features, std::vector<std::string>& responses)
    {
        int n = features.size();
        std::unordered_map<std::string, int> map;
        for (int i = 0; i < n; i++) map[features[i]] = 0;

        for (int i = 0; i < responses.size(); i++)
        {
            std::unordered_map<std::string, int> tmp;
            stringstream ss(responses[i]);
            string word;
            while (ss >> word)
            {
                if (map.find(word) != map.end())
                    tmp[word] = 1;
            }
            for (auto& [x, y] : tmp)map[x]++;
            tmp.clear();
        }

        std::stable_sort(features.begin(), features.end(), [&](const std::string& x, const std::string& y) {
            return map[x] > map[y];
            });

        return features;
    }
};