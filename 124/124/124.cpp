/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int ans;
    int maxPathSum(TreeNode* root)
    {
        ans = root->val;
        _maxPathSum(root);
        return ans;
    }


    int _maxPathSum(TreeNode* root)
    {
        if (root == nullptr) return 0;
        //当贡献值大于0才选取
        int left = max(_maxPathSum(root->left), 0);
        int right = max(_maxPathSum(root->right), 0);

        int tmp = root->val + left + right;
        ans = max(ans, tmp);

        return root->val + max(left, right);//往上返回时只能选取左右中的一边
    }
};