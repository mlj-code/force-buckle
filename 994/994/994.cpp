class Solution {
public:

    int orangesRotting(vector<vector<int>>& grid)
    {
        int m = grid.size(), n = grid[0].size();
        int cnt = 0;//记录新鲜橘子数
        auto judge = [&grid, m, n](int i, int j)
            {
                //位置合法且有橘子才能腐烂
                if (i >= 0 && j >= 0 && i < m && j < n && grid[i][j] == 1) return true;

                return false;
            };
        int time = 0;

        queue<pair<int, int>> rot;
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == 2) rot.push({ i,j });
                else if (grid[i][j] == 1) cnt++;
            }
        cout << cnt;
        while (!rot.empty())
        {
            time++;
            for (int ii = rot.size(); ii > 0; ii--)
            {
                int row = rot.front().first;
                int col = rot.front().second;
                rot.pop();

                if (judge(row + 1, col))
                {
                    cnt--;
                    grid[row + 1][col] = 2;//烂橘子
                    rot.push({ row + 1,col });
                }
                if (judge(row, col + 1))
                {
                    cnt--;
                    grid[row][col + 1] = 2;//烂橘子
                    rot.push({ row,col + 1 });
                }
                if (judge(row - 1, col))
                {
                    cnt--;
                    grid[row - 1][col] = 2;//烂橘子
                    rot.push({ row - 1,col });
                }
                if (judge(row, col - 1))
                {
                    cnt--;
                    grid[row][col - 1] = 2;//烂橘子
                    rot.push({ row,col - 1 });
                }
            }
        }

        if (cnt > 0) return -1;
        else return max(time - 1, 0);
    }
};