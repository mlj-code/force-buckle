class Solution {
public:
    double averageWaitingTime(vector<vector<int>>& customers)
    {
        double sum = 0;
        int n = customers.size();
        int pre = 0;
        for (int i = 0; i < n; i++)
        {
            double cur = max(pre, customers[i][0]);
            sum = sum + cur + customers[i][1] - customers[i][0];
            pre = cur + customers[i][1];
        }
        return sum / n;
    }
};