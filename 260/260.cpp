class Solution {
public:
    vector<int> singleNumber(vector<int>& nums)
    {
        int a = 0;
        for (auto v : nums)
        {
            a ^= v;
        }
        int b = 1;
        while ((a & b) == 0)
        {
            b <<= 1;
        }
        int ret1 = 0, ret2 = 0;
        for (auto v : nums)
        {
            if ((v & b) == 0)
            {
                ret1 ^= v;
            }
            else
            {
                ret2 ^= v;
            }
        }
        return vector<int>({ ret1,ret2 });

    }
};