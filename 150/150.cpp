class Solution {
public:
    int evalRPN(vector<string>& tokens)
    {

        map<string, function<int(int, int)>> m =
        {
            {"+",[](int x, int y) {return x + y; }},
            {"-",[](int x, int y) {return x - y; }},
            {"*",[](int x, int y) {return x * y; }},
            {"/",[](int x, int y) {return x / y; }},
        };
        if (tokens.size() == 1)
        {
            return stoi(tokens[0]);
        }

        stack<int> tmp;
        for (auto& e : tokens)
        {
            if (m.find(e) == m.end())
            {
                tmp.push(stoi(e));
            }
            else
            {
                int right = tmp.top(); tmp.pop();
                int left = tmp.top(); tmp.pop();
                tmp.push(m[e](left, right));
            }
        }

        return tmp.top();
    }
};