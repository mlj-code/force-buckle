typedef struct
{
    int top;
    int* a;
    int capcity;
}stack;

void StackInit(stack* stk)
{
    stk->top = 0;
    stk->capcity = 4;
    stk->a = (int*)malloc(sizeof(int) * 4);
}

//stk1��
//stk2��
typedef struct
{
    stack stk1;
    stack stk2;
} MyQueue;


void InStack(stack* stk, int x)
{
    if (stk->top == stk->capcity)
    {
        stk->a = (int*)malloc(sizeof(int) * stk->capcity * 2);
        stk->capcity = stk->capcity * 2;
    }
    stk->a[stk->top++] = x;
}

int StackTop(stack* stk)
{
    return stk->a[stk->top - 1];
}

void StackPop(stack* stk)
{
    stk->top--;
}

bool StackEmpaty(stack* stk)
{
    return stk->top == 0;
}

MyQueue* myQueueCreate()
{
    MyQueue* ret = (MyQueue*)malloc(sizeof(MyQueue));
    StackInit(&ret->stk1);
    StackInit(&ret->stk2);
    return ret;
}

void myQueuePush(MyQueue* obj, int x)
{
    InStack(&obj->stk1, x);
}

void Move(stack* src, stack* dest)
{
    int top = src->top;
    for (int i = 0; i < top; i++)
    {
        InStack(dest, StackTop(src));
        StackPop(src);
    }
}

int myQueuePop(MyQueue* obj)
{
    if (StackEmpaty(&obj->stk2))
    {
        Move(&obj->stk1, &obj->stk2);
    }
    int ret = StackTop(&obj->stk2);
    StackPop(&obj->stk2);
    return ret;
}

int myQueuePeek(MyQueue* obj)
{
    if (StackEmpaty(&obj->stk2))
    {
        Move(&obj->stk1, &obj->stk2);
    }

    int ret = StackTop(&obj->stk2);
    return ret;
}

bool myQueueEmpty(MyQueue* obj)
{
    if (StackEmpaty(&obj->stk1) && StackEmpaty(&obj->stk2))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void myQueueFree(MyQueue* obj)
{
    free(obj);
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/