class Solution {
public:
    bool isPalindrome(string s)
    {
        string::iterator it = s.begin();
        string ju;
        ju.reserve(s.size());
        while (it != s.end())
        {
            if (*it == ' ')
            {
                it++;
                continue;
            }
            else if (*it >= 'a' && *it <= 'z' || (*it >= '0' && *it <= '9'))
            {
                ju += *it;
                it++;
                continue;
            }
            else if (*it >= 'A' && *it <= 'Z')
            {
                ju += *it + 'a' - 'A';
                it++;
                continue;
            }
            else
            {
                it++;
                continue;
            }
        }
        int i = ju.size() / 2;
        int j = 0;
        int k = ju.size() - 1;
        while (i--)
        {
            if (ju[k] != ju[j])
            {
                return false;
            }
            j++; k--;
        }
        return true;
    }
};