#include <iostream>
#include <stack>
#include <algorithm>
#include <cmath>

using namespace std;

int _help(string& str)
{
    stack<int> st;
    string tmp;
    for (auto e : str)
    {
        if (e == '+')
        {
            st.push(atoi(tmp.c_str()));
            tmp.clear();
        }
        else if (e == '*')
        {
            int num = st.top();
            st.pop();
            num *= atoi(tmp.c_str());
            st.push(num);
            tmp.clear();
        }
        else tmp += e;
    }
    st.push(atoi(tmp.c_str()));

    int ret = 0;
    while (!st.empty())
    {
        ret += st.top();
        st.pop();
    }

    return ret;
}

bool help(string& str)
{
    int n = str.find('=');
    string x = str.substr(0, n);
    string y = str.substr(n + 1);

    if (_help(x) != _help(y)) return false;


    return true;
}

int main()
{
    string str;
    int n;
    cin >> n;
    while (cin >> str)
    {
        if (help(str) == true) cout << "Yes" << endl;
        else cout << "No" << endl;

    }

    return 0;
}