class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums)
    {
        vector<vector<int>> ret;
        sort(nums.begin(), nums.end());
        int n = nums.size();

        for (int i = 0; i < n; i++)
        {
            if (i != 0 && nums[i] == nums[i - 1])continue;
            int left = i + 1, right = n - 1;
            int target = -nums[i];
            while (left < right)
            {
                if (nums[left] + nums[right] == target)
                {
                    ret.push_back({ nums[i],nums[left],nums[right] });
                    while (left < right && nums[left] == nums[left + 1])left++;
                    while (left < right && nums[right] == nums[right - 1])right--;
                    left++, right--;
                }
                else if (nums[left] + nums[right] < target)
                {
                    left++;
                }
                else right--;
            }
        }
        return ret;
    }
};