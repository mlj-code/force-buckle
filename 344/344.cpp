class Solution {
public:
    void reverseString(vector<char>& s)
    {
        vector<char>::iterator end = s.end();
        vector<char>::iterator begin = s.begin();
        end--;
        int i = s.size() / 2;
        while (i--)
        {
            char tmp = *begin;
            *begin = *end;
            *end = tmp;
            begin++;
            end--;
        }
    }
};