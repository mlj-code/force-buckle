class Solution {
public:
    bool isReachableAtTime(int sx, int sy, int fx, int fy, int t)
    {
        if (sx == fx && sy == fy && t == 1) return false;

        int tmp = abs(sx - fx);
        tmp = max(tmp, abs(sy - fy));
        return t >= tmp;
    }
};