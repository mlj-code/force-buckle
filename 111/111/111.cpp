/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode* root)
    {
        if (root == nullptr) return 0;

        int left = minDepth(root->left);
        int right = minDepth(root->right);

        if (left == 0) return right + 1;
        else if (right == 0) return left + 1;//当左右有一颗是空树则不能返回空树的高度
        else return min(left, right) + 1;//只有当左右都不为空时才返回左右最小
    }
};