class Solution {
public:
    int maxPalindromesAfterOperations(vector<string>& words)
    {
        int n = words.size();
        int ret = n;
        int odd = 0, even = 0;
        unordered_map<char, int> map;
        sort(words.begin(), words.end(), [](const auto& x, const auto& y)
            {
                return x.size() > y.size();
            });
        for (int i = 0; i < n; i++)
        {
            if (words[i].size() % 2 == 0)even++;
            else odd++;
            for (int j = 0; j < words[i].size(); j++)
            {
                map[words[i][j]]++;
            }
        }
        int ch_odd = 0;
        for (auto& [ch, cnt] : map)
        {
            if (cnt % 2 == 1)ch_odd++;
        }
        ch_odd -= odd;//

        for (int i = 0; i < n && ch_odd>0; i++)
        {
            if (words[i].size() % 2 == 1)
            {
                ch_odd -= words[i].size() - 1;
            }
            else ch_odd -= words[i].size();
            ret--;
        }

        return ret;
    }
};