#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> findAnagrams(string s, string p)
    {
        vector<int> ret;
        unordered_map<char, int> map, tmp_map;
        int n = p.size(), left = 0, tmp = n;
        for (auto e : p)map[e]++;
        tmp_map = map;
        int i, j;
        for (i = 0; i < s.size(); i++)
        {
            bool flag = false;
            for (j = i; j < i + n; j++)
            {
                if (tmp_map.find(s[j]) != tmp_map.end())
                {
                    if (tmp_map[s[j]] > 0)
                    {
                        tmp_map[s[j]]--;
                        flag = true;
                    }
                    else//超次非法跳出,左边界由循环加一
                    {
                        flag = false;
                        break;
                    }
                }
                else
                {
                    i = j;//未出现非法跳出则需将左边界定位到非法处,再由循环加一
                    flag = false;
                    break;
                }
            }
            tmp_map = map;
            if (flag == true); ret.push_back(i);//合法跳出
        }
        return ret;
    }
};

int main()
{
    Solution().findAnagrams("cbaebabacd", "abc");
	return 0;
}