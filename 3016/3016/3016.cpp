class Solution {
public:
    struct cmp
    {
        bool operator()(const pair<char, int>& x, const pair<char, int>& y)
        {
            return x.second < y.second;
        }
    };
    int minimumPushes(string word)
    {
        unordered_map<char, int> map;
        for (auto ch : word)map[ch]++;

        priority_queue<pair<char, int>, vector<pair<char, int>>, cmp> q;
        for (auto& p : map) q.push(p);

        int cnt = 0;
        for (int i = 1; !q.empty(); i++)
        {
            for (int j = 0; j < 8 && !q.empty(); j++)
            {
                cnt += q.top().second * i;
                q.pop();
            }
        }

        return cnt;
    }
};