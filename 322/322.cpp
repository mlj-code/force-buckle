#if 0//优化前

class Solution {
public:
    int coinChange(vector<int>& coins, int amount)
    {
        int n = coins.size();
        vector<vector<int>> dp(n + 1, vector<int>(amount + 1, 0));

        for (int i = 1; i <= amount; i++)dp[0][i] = INT_MAX;//将不合法状态设为特殊值

        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= amount; j++)
            {
                dp[i][j] = dp[i - 1][j];;//没拿这个硬币
                //保证前面状态合法才继续
                if (j >= coins[i - 1] && dp[i][j - coins[i - 1]] != INT_MAX)
                {
                    dp[i][j] = min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
                }
            }
        }
        return dp[n][amount] == INT_MAX ? -1 : dp[n][amount];
    }
};

#else //优化后

class Solution {
public:
    int coinChange(vector<int>& coins, int amount)
    {
        int n = coins.size();
        vector<int> dp(amount + 1, INT_MAX);

        dp[0] = 0;
        for (int i = 1; i <= n; i++)
        {
            for (int j = coins[i - 1]; j <= amount; j++)
            {
                if (dp[j - coins[i - 1]] != INT_MAX)
                {
                    dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
                }
            }
        }

        return dp[amount] == INT_MAX ? -1 : dp[amount];
    }
};

#endif