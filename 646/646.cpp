class Solution {
public:
    int findLongestChain(vector<vector<int>>& p)
    {
        int n = p.size();
        sort(p.begin(), p.end());//保证以某个位置为结尾时,所有可链接在该位置之前的数对都在前面
        int ret = 1;
        vector<int> dp(n, 1);
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (p[i][0] > p[j][1])
                {
                    dp[i] = max(dp[j] + 1, dp[i]);
                }
                ret = max(ret, dp[i]);
            }
        }
        return ret;
    }
};