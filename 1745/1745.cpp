class Solution {
public:
    bool checkPartitioning(string s)
    {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));

        for (int i = 0; i < n; i++)
        {
            for (int j = i; j >= 0; j--)
            {
                if (s[i] == s[j])
                {
                    dp[i][j] = j + 1 < i ? dp[i - 1][j + 1] : true;
                }
            }
        }

        for (int i = 1; i < n - 1; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (dp[j][0] && dp[i][j + 1] && dp[n - 1][i + 1])
                    return true;
            }
        }
        return false;
    }
};