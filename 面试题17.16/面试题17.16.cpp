class Solution {
public:
    int massage(vector<int>& nums)
    {
        int n = nums.size();
        if (n == 0) return 0;
        vector<int> f(n, 0);//选当前位置的最大时长
        vector<int> g(n, 0);//不选当前位置的最大时长
        f[0] = nums[0];
        g[0] = 0;
        for (int i = 1; i < n; i++)
        {
            f[i] = nums[i] + g[i - 1];
            g[i] = max(g[i - 1], f[i - 1]);//没选当前位置则等于上一个位置选没选的最大值
        }
        return max(f[n - 1], g[n - 1]);
    }
};