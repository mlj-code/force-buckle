class Solution {
public:
    int deleteAndEarn(vector<int>& nums)
    {

        int m[10001] = { 0 };
        int end = 0;
        for (auto n : nums)
        {
            m[n]++;
            if (n > end) end = n;
        }
        int f[20001] = { 0 };
        int g[20001] = { 0 };
        f[1] = m[1]; g[1] = 0;
        for (int i = 2; i < 10001; i++)
        {
            f[i] = m[i] * i + g[i - 1];
            g[i] = max(g[i - 1], f[i - 1]);
        }
        return max(f[end], g[end]);
    }
};