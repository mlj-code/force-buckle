#include <iostream>
#include <vector>
using namespace std;

vector<int> arr(1001);
int n;
int dp[1005];

int help(int num)
{
    int ret = 0;

    for (int i = 0; i <= num; i++)
    {
        dp[i] = 1;
        for (int j = 0; j < i; j++)
        {
            if (arr[i] > arr[j]) dp[i] = max(dp[i], dp[j] + 1);
        }
    }
    ret += dp[num];
    for (int i = n - 1; i >= num; i--)
    {
        dp[i] = 1;
        for (int j = n - 1; j > i; j--)
        {
            if (arr[i] > arr[j]) dp[i] = max(dp[i], dp[j] + 1);
        }
    }
    ret += dp[num];
    return n - ret + 1;
}

int main()
{
    cin >> n;

    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    int ans = 1000;

    for (int i = 0; i < n; i++)
    {
        ans = min(ans, help(i));
    }

    cout << ans << endl;
}
