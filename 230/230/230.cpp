/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int ret;
    int kthSmallest(TreeNode* root, int k)
    {
        _inorder(root, k);
        return ret;
    }
    //�������
    void _inorder(TreeNode* root, int& k)
    {
        if (root == nullptr) return;
        _inorder(root->left, k);
        k--;
        if (k == 0)
        {
            ret = root->val;
            return;
        }
        _inorder(root->right, k);
    }
};