class Solution {
public:
    int lengthOfLongestSubstring(string s)
    {
        unordered_map<char, int> map;
        int left = 0, ret = 0;
        for (int i = 0; i < s.size(); i++)
        {
            if (map.find(s[i]) == map.end() || map[s[i]] < left)
            {
                //找不到就插入,左边界不变,当找到的值小于左边界也无效
                map[s[i]] = i;
                ret = max(ret, i - left + 1);
            }
            else
            {
                //找到了就更新左边界
                left = map[s[i]] + 1;
                map[s[i]] = i;
            }
        }
        return ret;
    }
};